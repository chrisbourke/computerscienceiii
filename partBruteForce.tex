%!TEX root = ComputerScienceThree.tex

\section{Introduction}

\begin{itemize}
  \item Brute Force style algorithms are simple a ``just do it'' approach
  \item Usually an ``obvious'', straightforward solution
  \item Not ideal and may be completely infeasible for even small inputs
  \item May be necessary (ex: sequential search, max/min finding)
\end{itemize}

\subsection{Examples}

Shuffling Cards
\begin{itemize}
  \item There are $52! \approx 8.065817 \times 10^{67}$ possible shuffles
  \item If 5 billion people shuffled once per second for the last 1,000 years, only:
	$$1.95 \times 10^{-48} = 0.\underbrace{00 \ldots 00}_{45 \text{ zeros}}195\%$$
	have been examined so far!
  \item Even at 5 billion shuffles per second, it would take
	$$5.1118 \times 10^{50}$$
	years to enumerate all possibilities
\end{itemize}

Greatest Common Divisor
\begin{itemize}
  \item Given two integers, $a, b \in \mathbb{Z}^+$, the \emph{greatest common divisor}:
	$$\mathrm{gcd}(a, b)$$
	is the largest integer that divides both $a$ and $b$
  \item Naive solution: factor $a, b$:
	$$\begin{array}{rcl}
	  a & = & p_1^{k_1}p_2^{k_1}\cdots p_n^{k_n} \\
	  b & = & p_1^{\ell_1}p_2^{\ell_1}\cdots p_n^{\ell_n} \\
	\end{array}$$
  \item Then
	$$\mathrm{gcd}(a, b) = p_1^{\min\{k_1, \ell_1\}}p_2^{\min\{k_2, \ell_2\}}\cdots p_n^{\min\{k_n, \ell_n\}}$$
  \item Factoring is not known (or believed) to be in $\P$ (deterministic polynomial time)
  \item Better solution: Euclid's GCD algorithm
\end{itemize}

\subsection{Backtracking}

\begin{itemize}
  \item We iteratively (or recursively) build \emph{partial} solutions such that solutions can be ``rolled back'' to a prior state
  \item Once a full solution is generated, we process/test it then roll it back to a (previous partial) solution
  \item Advantage: depending on the nature/structure of the problem, entire branches of the computation tree may be ``pruned''
	out
  \item Avoiding infeasible solutions can greatly speed up computation in practice
  \item Heuristic speed up only: in the worst case and in general, the computation may still be exponential
\end{itemize}

\section{Generating Combinatorial Objects}

\subsection{Generating Combinations (Subsets)}

Recall that combinations are simply all possible subsets of size
$k$.  For our purposes, we will consider generating subsets of
  $$\{1, 2, 3, \ldots, n\}$$

This is enough because the combination (or permutation) of integers
can be used as indices for other objects stored in an array/list.  
Using the indices you can build the combination/permutation of 
objects using the indices.

The idea is simple: given a combination, find the right most
value that has not yet reached its highest possible value (we're
essentially counting but not in the normal base 10 way).  
Then increment it to the next value.  All the values to the 
left are then reset to the lowest possible values.  

For example, if $n = 10, k = 5$, consider the combination:
$$\{2, 5, 6, 9, 10\}$$
(which has been ordered for the purposes of this algorithm, but
combinations are unordered in general).  The last element, 10
has reached the highest possible value, so we can't increase it
any more.  Likewise, 9, though not the highest possible value
overall is still as high as it can be because if it were any
larger, then we'd have a duplicate of 10.  6 is does \emph{not}
have its highest possible value because 7 and 8 are both 
missing from the combination.  6 is the value we increase (to 7)
and then we set all the remaining values to their lowest possible
values which are 8 and 9 (the two that follow the new value 7.
This gives the next combination: 
$$\{2, 5, 7, 8, 9\}$$


The algorithm works as follows.

    \begin{itemize}
     \item Start with $\{1, \ldots, k\}$
     \item Assume that we have $a_1a_2\cdots a_k$, we want the next
     combination
     \item Locate the last element $a_i$ such that $a_i \neq n - k + i$
     \item Replace $a_i$ with $a_i + 1$
     \item Replace $a_j$ with $a_i + j - i$ for $j = i+1, i+2, \ldots, k$
    \end{itemize}

    \begin{algorithm}[H]
    \Input{A set of $n$ elements and an $k$-combination, $a_1\cdots a_k$.}
    \Output{The next $k$-combination.}
     $i = k$ \;
     \While{$a_i = n - k + i$}{
      $i = i-1$ \;
      }
     $a_i = a_i + 1$ \;
     \For{$j = (i+1)\ldots k$}{
       $a_j = a_i + j - i$ \;
       }
    \caption{Next $k$-Combination}
    \label{algo:nextCombination}
    \end{algorithm}

Example: Find the next 3-combination of the set $\{1, 2, 3, 4, 5\}$ after $\{1, 4, 5\}$

Here, $n=5, k=3$, $a_1 = 1, a_2 = 4, a_3 = 5$.

The last $i$ such that $a_i \neq 5 - 3 + i$ is 1.

    Thus, we set
    $$\begin{array}{rcl}
     a_1 & = & a_1 + 1 = 2 \\
     a_2 & = & a_1 + 2 - 1 = 3 \\
     a_3 & = & a_1 + 3 - 1 = 4
     \end{array}$$

    So the next $k$-combination is $\{2, 3, 4\}$.

Two full examples are shown in Figure \ref{figure:combinationSequences}.

\begin{figure}
\centering
\subfigure[Sequence for ${5 \choose 3}$]{
\begin{tabular}{cc}
Iteration & Combination \\
\hline\hline
1 & 123 \\
2 & 124 \\
3 & 125 \\
4 & 134 \\
5 & 135 \\
6 & 145 \\
7 & 234 \\
8 & 235 \\
9 & 245 \\
10 & 345 \\
\end{tabular}
}\subfigure[Sequence for ${6 \choose 4}$]{
\begin{tabular}{cc}
Iteration & Combination \\
\hline\hline
1 & 1234\\
2 & 1235\\
3 & 1236\\
4 & 1245\\
5 & 1246\\
6 & 1256\\
7 & 1345\\
8 & 1346\\
9 & 1356\\
10 & 1456\\
11 & 2345\\
12 & 2346\\
13 & 2356\\
14 & 2456\\
15 & 3456\\
\end{tabular}

}
\caption{Two combination sequence examples}
\label{figure:combinationSequences}
\end{figure}

You can also use Gosper's Hack: \url{http://andy.kitchen/combinations.html}

\subsection{Generating Permutations}

\begin{itemize}
  \item A \emph{permutation} of $n$ elements, $a_1, \ldots, a_n$ is an ordered arrangement of these elements
  \item There are $n!$
  \item The Johnson-Trotter algorithm generates all permutations with the
  	\emph{least change} property (at most two elements are swapped in each
	permutation).
\end{itemize}

Lexicographically Ordered Permutations:
\begin{itemize}
  \item Start with permutation $1234\cdots n$
  \item Given a current permutation, $a_1, a_2, \ldots, a_n$
  \item Find the right-most pair $a_i, a_{i+1}$ such that $a_i < a_{i+1}$
  \item Find the smallest element (to the right, ``in the tail'') larger than $a_i$, label it $a'$
  \item Swap $a'$ and $a_i$
  \item Order (sort) elements to the right of $a'$
  \item Example: $163542 \rightarrow 164235$
  \item $35$ is the last pair; 4 is the minimal element (among 5, 4, 2) greater than $a_i = 3$;
  	swap $3, 4$ giving $164532$; sort $532$ to $235$ giving the result  	
  \item Example: $12345 \rightarrow 12354 \rightarrow 12435 \rightarrow \cdots$
\end{itemize}

Generalization:
\begin{itemize}
  \item In general, permutations can be formed from subsets of a set
  \item The number ordered permutations of $k$ elements from a set of $n$ distinct elements is:
  	$$P(n, k) = \frac{n!}{(n-k)!}$$
  \item We can adapt the two previous algorithms to generate these permutations: use the combinations 
  	algorithm to generate all distinct subsets of size $k$; then run the permutation algorithm on each 
	subset to generate ordered permutations.
\end{itemize}

    \begin{algorithm}
    \Input{A permutation $a_1a_2\dots a_n$ of elements $1, 2, \ldots, n$}
    \Output{The lexicographically next permutation}
     $i \leftarrow n-1$ \;
     \While{$a_i > a_{i+1}$}{
       $i \leftarrow (i-1)$ \;
     }
     $a' \leftarrow a_{i+1}$ \;
     \For{$j = (i+2)\ldots n$}{
       \If{$a_j > a_i \wedge a_j < a'$}{
         $a' \leftarrow a_j$ \;
       }
     }
     swap $a', a_i$ \;
     sort elements $a_{i+1}\cdots a_n$ \;
    \caption{Next Lexicographic Permutation}
    \label{algo:nextLexicoPermutation}
    \end{algorithm}

\subsection{Permutations with Repetition}

\begin{itemize}
  \item Let $A = \{a_1, \ldots, a_n\}$ be a set with $n$ objects
  \item We wish to form an ordered permutation of length $k$ of elements from $A$ but we are also allowed to take as many ``copies'' of each element as we wish
  \item Number of such permutations: $n^k$
  \item Use case: generating DNA sequences from amino acid bases, $\{A, G, C, T\}$
  \item Straightforward idea: count in base $b = n$ and associate each number in base $b$ with an element in $A$
  \item Count from $0$ to $n^k -1$ and generate elements
  \item A direct counting algorithm is presented as Algorithm \ref{algo:repeatedPermutation}
  \item A general purpose algorithm for base-conversion is presented as Algorithm \ref{algo:baseConversion}
\end{itemize}


  \begin{algorithm}[H]
    \Input{A base set $A = \{a_1, \ldots, a_n\}$ and a current permutation (base-$n$ number) $x_1 \cdots x_k$}
    \Output{The next permutation}
    $i \leftarrow k$ \;
    $carry \leftarrow true$ \;
    \While{$carry \wedge i \geq 1$}{
      $x_i \leftarrow x_i + 1$ \;
      \eIf{$x_i \geq n$}{
        $x_i \leftarrow 0$ \;
        $i \leftarrow i + 1$ \;
      }{
        $carry \leftarrow false$ \;
      }
    }
    output $x_1 \cdots x_k$ \;
    \caption{Next Repeated Permutation Generator}
    \label{algo:repeatedPermutation}
  \end{algorithm}
  
  
  \begin{algorithm}[H]
    \Input{$n \in \mathbb{N}$, an ordered symbol set, $\Sigma=\{\sigma_0, \sigma_1, \ldots, \sigma_{b-1}\}$, a base $b$}
    \Output{The base-$b$ representation of $n$, $x_k\ldots x_1x_0$ using the symbol set $\Sigma$}    
    $i \leftarrow 0$ \;
    \While{$n > 0$}{
      $j \leftarrow n \mod b$ \;
      $n \leftarrow \lfloor \frac{n}{b} \rfloor$ \;
      $x_i \leftarrow \sigma_j$ \;
      $i \leftarrow (i+1)$ \;
    }
    optionally: pad out $x$ with leading $\sigma_0$ symbols (leading zeros) until it has the desired length\;
    output $x_k \cdots x_1x_0$ \;
    \caption{Base Conversion Algorithm}
    \label{algo:baseConversion}
  \end{algorithm}

\subsection{Set Partitions}

Let $A = \{a_1, \ldots a_n\}$ be a set.  A \emph{partition} of $A$ is a collection of disjoint subsets of $A$
whose union is equal to $A$.  That is, a partition splits up the set $A$ into a collection of subsets.  The number
of ways to partition a set corresponds to the \emph{Bell numbers}:

$$B_{n+1} = \sum_{k=0}^n {n \choose k} B_k$$
with $B_0 = 1$.

  \begin{algorithm}[H]
    \Input{A set $A = \{a_1, \ldots, a_n\}$}
    \Output{A set of all set partitions of $A$}
    $partitions \leftarrow \{\{a_1\}\}$ \;
    \For{$i=2, \ldots, |A|$}{
      $newPartitions \leftarrow \emptyset$ \;
      \ForEach{partition $part \in partitions$}{
        \ForEach{set $s \in part$}{
          $newPart \leftarrow copy of part$ \;
          add $a_i$ to $s$ in $newPart$ \;
          add $newPart$ to $newPartitions$ \;
        }
        $newPart \leftarrow copy of part$ \;
        add $\{a_i\}$ to $newPart$ \;
      }
      $partitions \leftarrow newPartitions$ \;
    }
    output $partitions$ \;
    \caption{Set Partition Generator}
    \label{algo:setPartitionGenerator}
  \end{algorithm}

Demonstration: Starting with $\{\{a, b\}, \{\{a\}, \{b\}\}\}$ (which partitions the set $A=\{a, b\}$ into
two possible partitions), we can produce all partitions of the set $A' = \{a, b, c\}$ by adding $c$
to each of the possible subsets of subsets (or in a set by itself):

$$\begin{array}{c}
\big\{
\{ \{a\}, \{b\}, \{c\} \}, \\
\{ \{a\}, \{b, c\} \}, \\
\{ \{b\}, \{a, c\} \}, \\
\{ \{c\}, \{a, b\} \}, \\
\{ \{a, b, c\} \} \big\}
\end{array}
$$

\section{Problems \& Algorithms}

\subsection{Satisfiability}
\index{Satisfiability}\index{Problems!Satisfiability}

\begin{itemize}
  \item Recall that a \emph{predicate} is a boolean function on $n$ variables:
	$$P(\vec{x}) = P(x_1, \ldots, x_n)$$
  \item A predicate is \emph{satisfiable} if there exists an assignment of variables that makes $P$ evaluate to true:
	$$\exists x_1, \ldots, x_n \left[ P(x_1, \ldots, x_n) \right]$$
  \item Example: 
  	$$(x_1 \vee \neg x_2) \wedge (\neg x_1 \vee x_2)$$
	is satisfiable (there are two assignments for which the exclusive-or evaluates to true)
  \item Example:
  	$$(x_1 \vee x_2 \vee x_3) \wedge (x_1 \vee \neg x_2) \wedge (x_2 \vee \neg x_3) \wedge (x_3 \vee \neg x_1) \wedge (\neg x_1 \vee \neg x_2 \vee \neg x_3)$$
	is \emph{not} satisfiable: any setting of the three variables evaluates to false
  \item Deciding if a given predicate is satisfiable or not is a fundamental $\NP$-complete problem
  \item Determining if something is satisfiable only requires finding one
  truth assignment, but determining if it is not satisfiable may require trying
  all possibilities 
  \item However, we may be able to cut down on our work.  (An artificial)
  example:
  $$(x_1 \vee x_2 \vee x_3) \wedge (\neg x_1 \vee x_2 \vee \neg x_3) \wedge (\neg x_1 \vee x_2 \vee x_3)$$
  \item Setting $x_2 = 0$ would force the predicate to be false regardless of the
  truth values of $x_1, x_3$ (of which there are 4), so we don't have to consider
  any of these possibilities.  Any (partial) solution that sets $x_2 = 0$ can be
  ``pruned'' and ignored.
\end{itemize}



\begin{problem}[Satisfiability]
  \label{problem:satisfiability}
  ~\\
  \textbf{Given:} A boolean predicate $P$ on $n$ variables \\
  \textbf{Output:} \textsc{True} if $P$ is satisfiable, false otherwise
\end{problem}

  \begin{algorithm}[H]
    \Input{A predicate $P(x_1, \ldots, x_n)$}
    \Output{true if $P$ is satisfiable, false otherwise}
    \ForEach{truth assignment $\vec{t}$}{
      \If{$P(\vec{t}) = 1$}{
        output \textsc{True} \;
      }
    }
    output \textsc{False} \;
    \caption{Brute Force Iterative Algorithm for Satisfiability}
    \label{algo:bruteForceSat}
  \end{algorithm}

  \begin{algorithm}[H]
    \Input{A predicate $P(x_1, \ldots, x_n)$, a partial truth assignment $\vec{t} = t_1, \ldots, t_k$}
    \Output{true if $P$ is satisfiable, false otherwise}
    \eIf{$k = n$}{
      return $P(t_1, \ldots, t_n)$ \;
    }{
      $t_{k+1} \leftarrow 0$ \;
      \eIf{$\textsc{Sat}(P, \vec{t})$}{
        return \textsc{True};
      }{
       $t_{k+1} \leftarrow 1$ \;
       return $\textsc{Sat}(P, \vec{t})$ \;
      }
    }
    \caption{Brute Force Recursive Algorithm for Satisfiability}
    \label{algo:bruteForceRecSat}
  \end{algorithm}
  

\begin{itemize}
  \item Algorithm \ref{algo:bruteForceRecSat} prunes the tree in the sense that if it
  finds a satisfying assignment, the algorithm terminates without having to explore
  the remaining truth assignments.
  \item Additional pruning is possible if we have a subroutine that considers a 
  partial truth assignment and determines if it is true, false, or indeterminate.
  For example, it could look at each clause and: 
  \begin{itemize}
    \item If any one evaluates to false, it doesn't matter what the remaining (undetermined) truth values are, the current (partial) assignment is not satisfying and it can return false.
    \item If the partial truth assignment satisfies all clauses, then the remaining truth variables are irrelevant and can be set to anything and we return true
    \item Otherwise, the algorithm can return ``don't know'' as the partial assignment does not give enough information to determine if it is satisfiable or not.
  \end{itemize}
  \item Suppose we have such a subroutine, $\textsc{PartialSat}$ that takes a predicate and a partial assignment, $\vec{t}$ and returns:
  $$\textsc{PartialSat}(P, \vec{t}) = \left\{\begin{array}{ll}
  0 & P(\vec{t}) = 0\\
  1 & P(\vec{t}) = 1\\
  \phi & \textrm{otherwise}
  \end{array}\right.$$
  \item We can then use this subroutine to perform more pruning as in Algorithm \ref{algo:bruteForceRecSatPruning}
\end{itemize}  
  
  \begin{algorithm}[H]
    \Input{A predicate $P(x_1, \ldots, x_n)$, a partial truth assignment $\vec{t} = t_1, \ldots, t_k$}
    \Output{true if $P$ is satisfiable, false otherwise}
    \eIf{$k = n$}{
      return $P(t_1, \ldots, t_n)$ \;
    }{
      \uIf{$\textsc{PartialSat}(P, \vec{t}) = 1$}{
        \Comment{Pruning, the remaining truth values are irrelevant}
        return \textsc{True}\;
      }
      \uElseIf{$\textsc{PartialSat}(P, \vec{t}) = 0$}{
        \Comment{Pruning, the remaining truth values cannot lead to a satisfying assignment}
        return \textsc{False}\;
      }
      \uElseIf{$\textsc{PartialSat}(P, \vec{t}) = \phi$}{
        \Comment{The predicate may still be satisfiable or not, we need to continue the computation}
        $t_{k+1} \leftarrow 0$ \;
        \eIf{$\textsc{Sat}(P, \vec{t})$}{
          return \textsc{True};
        }{
          $t_{k+1} \leftarrow 1$ \;
          return $\textsc{Sat}(P, \vec{t})$ \;
        }
      }      
    }
    \caption{Brute Force Recursive Algorithm for Satisfiability With Pruning}
    \label{algo:bruteForceRecSatPruning}
  \end{algorithm}

  

\input{figures/figSatisfiabilityTree}

\subsection{Hamiltonian Path/Cycle}
\index{Hamiltonian Path}
\index{Hamiltonian Cycle}

\index{Neighborhood}
\begin{definition}
  Let $G=(V,E)$ be an undirected graph and let $v \in V$ be a vertex.  The \emph{neighborhood} of $v$, $N(v)$ is the
  set of vertices connected to $v$.
	$$N(v) = \{x | (u,v) \in E\}$$
\end{definition}

\index{Problems!Hamiltonian Path}
\begin{problem}[Hamiltonian Path]
  \label{problem:hamiltonianPath}
  ~\\
  \textbf{Given:} An undirected graph $G=(V,E)$ \\
  \textbf{Output:} \textsc{True} if $G$ contains a Hamiltonian Path, false otherwise
\end{problem}

Variations:
\begin{itemize}
  \item Functional version: output a Hamiltonian path if one exists
  \item Hamiltonian Cycle (Circuit) problem
  \item Weighted Hamiltonian Path/Cycle
  \item Traveling Salesperson Problem
\end{itemize}

\begin{problem}[Traveling Salesperson (TSP)]
  \label{problem:travelingSalesperson}
  ~\\
  \textbf{Given:} A collection of $n$ cities, $C = \{c_1, \ldots, c_n\}$\\
  \textbf{Output:} A permutation of cities $\pi(C) = (c_1', c_2', \ldots, c_n')$ such that the total distance,
	$$\left(\sum_{i=1}^{n-1} \delta(c_i', c_{i+1}')\right) + \delta(c_n', c_1')$$
	is minimized.
\end{problem}

Discussion: is one variation ``harder'' than the other?

\begin{itemize}
  \item Contrast two brute force strategies
  \item Backtracking enables \emph{pruning} of recurrence tree
  \item potential for huge heuristic speed up
\end{itemize}

  \begin{algorithm}[H]
    \Input{A graph, $G=(V,E)$, a path $p = v_1, \ldots, v_k$}
    \Output{true if $G$ contains a Hamiltonian cycle, false otherwise}
    \ForEach{permutation of vertices $pi = v_1, \ldots, v_n$}{
      $isHam \leftarrow true$ \;
      \For{$i=1, \ldots, n-1$}{
        \If{$(v_i, v_{i+1}) \not\in E$}{
          $isHam \leftarrow false$ \;
        }
      }
      \If{$(v_n, v_{1}) \not\in E$}{
        $isHam \leftarrow false$ \;
      }
      \If{$isHam$}{
        return \textsc{True} \;
      }
    }
    return \textsc{False} \;
    \caption{Brute Force Iterative Algorithm for Hamiltonian Cycle}
    \label{algo:bruteForceSat}
  \end{algorithm}

  \begin{algorithm}[H]
    \Input{A graph, $G=(V,E)$, a path $p = v_1, \ldots, v_k$}
    \Output{true if $G$ contains a Hamiltonian cycle, false otherwise}
    \If{$|p| = n$}{
      return \textsc{true} \;
    }
    \ForEach{ $x \in N(v_k)$ } {
      \If{$x \not\in p$}{
        \If{$\textsc{Walk}(G, p + x)$} {
          return \textsc{true} \;
        }
      }
    }
    return \textsc{true} \;
    \caption{Hamiltonian DFS Cycle Walk}
    \label{algo:hamCycleWalk}
  \end{algorithm}

\newpage

  \begin{algorithm}[H]
    \Input{A graph, $G=(V,E)$}
    \Output{\textsc{True} if $G$ contains a Hamiltonian path, \textsc{False} otherwise}
    \ForEach{ $v \in V$ } {
      $path \leftarrow v$ \;
      \If{$\textsc{Walk}(G, p)$}{
        output \textsc{True} \;
      }
      output \textsc{False} \;
    }
    return \textsc{True} \;
    \caption{Hamiltonian DFS Path Walk--Main Algorithm}
    \label{algo:hamPathWalkMain}
  \end{algorithm}

  \begin{algorithm}[H]
    \Input{A graph, $G=(V,E)$, a path $p = v_1\cdots v_k$}
    \Output{\textsc{True} if $G$ contains a Hamiltonian path, \textsc{False} otherwise}
    \If{$k = n$}{
      return \textsc{True} \;
    }
    \ForEach{ $v \in N(v_k)$ }{
      \If{$v \not\in p$}{
        $\textsc{Walk}(G, p + v)$ \;
      }
    }
    return \textsc{False} \;
    \caption{$\textsc{Walk}(G, p)$ -- Hamiltonian DFS Path Walk}
    \label{algo:hamPathWalkSub}
  \end{algorithm}

\begin{itemize}
  \item To see how the walk approach can provide a substantial speed up observe the graph if Figure \ref{fig:hamiltonianGraph}
  \item Any permutation of vertices that involves edges that are not present need not be processed
  \item Backtracking computation is illustrated in Figure \ref{fig:hamPathTraversals}
\end{itemize}

\begin{figure}[h]
\centering
\input{figures/figHamiltonianGraph}
\caption{A small Hamiltonian Graph}
\label{fig:hamiltonianGraph}
\end{figure}

\input{figures/figHamTraversals}

\subsection{0-1 Knapsack}

\begin{problem}[0-1 Knapsack]
  \label{problem:zeroOneKnapsack}
  ~\\
  \textbf{Given:} A collection of $n$ items, $A = \{a_1, \ldots, a_n\}$, a weight function
	$wt: A \rightarrow \mathbb{R}+$, a value function $val: A \rightarrow \mathbb{R}^+$ and
	a knapsack capacity $W$\\
  \textbf{Output:} A subset $S \subseteq A$ such that
	$$wt(S) = \sum_{s\in S} wt(s) \leq W$$
	and
	$$val(S) = \sum_{s\in S} val(s)$$
	is maximized.
\end{problem}

An example input can be found in Figure \ref{figure:exampleKnapsackInput}.  The optimal solution is to take items $a_1, a_3, a_4$ for
a value of 29.  Another example can be found in Table \ref{table:dynamicKnapsackInput} in a later section where we look at a dynamic
programming solution for the same problem.

\begin{figure}[h]
\centering
\begin{tabular}{lll}
Item & Value & Weight \\
\hline\hline
$a_1$ & 15 & 1 \\
$a_2$ & 10 & 5 \\
$a_3$ & 9 & 3 \\
$a_4$ & 5 & 4 \\
\end{tabular}
\caption{Example Knapsack Input with $W = 8$}
\label{figure:exampleKnapsackInput}
\end{figure}

\begin{itemize}
  \item We wish to maximize the value of items (in general, an ``objective function'') taken subject to some constraint
  \item 0-1 refers to the fact that we can an item or leave it
  \item Variation: the \emph{dual} of the problem would be to minimize some \emph{loss} or \emph{cost}
  \item Variation: fractional knapsack
  \item A greedy strategy fails in general; a small example:
    three items with weights $1, 2, 3$ and values $6, 10, 12$
    respectively; and a total weight capacity of $4$.  The
    ratios would be $6, 5, 4$ respectively meaning that a
    greedy strategy would have us select the first two items
    for a total weight of $3$ and value of $16$.  However, the
    optimal solution would be to select the first and third item
    for a total weight of $4$ and value of $18$.
  \item A brute-force backtracking algorithm is presented as Algorithm \ref{algo:backtrackingKnapsack}; note that you would
    invoke this algorithm with $j = 0$ and $S = \emptyset$ initially.
\end{itemize}

\begin{algorithm}[H]
  \Input{An instance of the knapsack problem $K=(A, wt, val, W)$, an index $j$, and a partial solution $S \subseteq A$ consisting of
  elements not indexed more than $j$}
  \Output{A solution $S'$ that is at least as good as $S$}

  \If{$j = n$}{
    return $S$ \;
  }
  $S_{best} \leftarrow S$ \;
  \For{$k = j+1, \ldots, n$}{
    $S' \leftarrow S \cup \{a_k\}$ \;
    \If{$wt(S') \leq W$}{
      $T \leftarrow \textsc{Knapsack}(K, k, S')$ \;
      \If{$val(T) > val(S_{best})$}{
        $S_{best} \leftarrow T$ \;
      }
    }
  }
  return $S_{best}$ \;
  \label{algo:backtrackingKnapsack}
  \caption{$\textsc{Knapsack}(K, S)$ -- Backtracking Brute Force 0-1 Knapsack}
\end{algorithm}

\begin{figure}[h]
\centering
\input{figures/figKnapsackTree.tex}
\caption{Knapsack Computation Tree for $n = 4$}
\label{fig:knapsackTree}
\end{figure}

\subsection{Closest Pair}
\label{subsection:ClosestPair}

\begin{problem}[Closest Pair]
  \label{problem:closestPair}
  ~\\
  \textbf{Given:} A collection of $n$ points, $P = \{p_1 = (x_1, y_2), \ldots, p_n = (x_n, y_n)\}$\\
  \textbf{Output:} A pair of points $(p_a, p_b)$ that are the closest according to Euclidean distance.
\end{problem}

\subsection{Convex Hull}

\begin{itemize}
  \item A set of point $P$ in the plane is \emph{convex} if for any pair of points $p, q \in P$ every point
	in the line segment $\overline{pq}$ is contained in $P$
  \item A \emph{convex hull} of a set of points $P$ is the smallest convex set containing $P$
\end{itemize}

\begin{problem}[Convex Hull]
  \label{problem:convexHull}
  ~\\
  \textbf{Given:} A set $P$ of $n$ points \\
  \textbf{Output:} A convex hull $H$ of $P$
\end{problem}

\begin{itemize}
  \item The idea is to choose \emph{extreme points}: points that when connected form the border of the convex hull
  \item A pair of points $p_1 = (x_1, y_1), p_2 = (x_2, y_2)$ are \emph{extreme points} if all other $p \in P \setminus \{p_1, p_2\}$
	lie on the same side of the line defined by $p_1, p_2$
\end{itemize}

  \begin{algorithm}[H]
    \Input{A set of points $P \subseteq \mathbb{R}^2$}
    \Output{A convex hull $H$ of $P$}
    $H \leftarrow \emptyset$ \;
    \ForEach{pair of points $p_i, p_j$}{
      $m \leftarrow \frac{y_j - y_i}{x_j - x_i}$ \;
      $b \leftarrow y_i - m \cdot x_i$ \Comment{$m, b$ define the line $\overline{p_ip_j}$}
      $p_k \leftarrow $ an arbitrary point in $P \setminus \{p_i, p_j\}$ \;
      $s \leftarrow \mathrm{sgn}(y_k - m\cdot x_k - b)$ \Comment{$s$ indicates which side of the line $p_k$ lies on: positive for above, negative for below}
      $isExtreme \leftarrow true$ \;
      \ForEach{$p=(x,y) \in P \setminus \{p_i, p_j\}$}{
        \If{$s \neq \mathrm{sgn}(y - m\cdot x - b)$}{
          $isExtreme \leftarrow false$ \;
        }
      }
      \If{$isExtreme$}{
        $H \leftarrow H \cup \{p_1, p_2\}$ \;
      }
    }
    output $H$ \;
    \caption{Brute Force Convex Hull}
    \label{algo:convexHull}
  \end{algorithm}

\subsection{Assignment Problem}

\begin{problem}[Assignment]
  \label{problem:assignment}
  ~\\
  \textbf{Given:} A collection of $n$ tasks $T = \{t_1, \ldots, t_n\}$ and $n$ persons $P = \{p_1, \ldots, p_n\}$ and an $n \times n$
	matrix $C$ with entries $c_{i,j}$ representing the \emph{cost} of assigning person $p_i$ to task $t_j$.\\
  \textbf{Output:} A bijection $f: P \rightarrow T$ such that
	$$\sum_{p_i \in P} C[i, f(p_i)]$$
	is minimized.
\end{problem}

A brute force approach: try all bijections $f: T\rightarrow P$, equivalent to all permutations of one of these sets.

This is actually efficiently solvable via the Hungarian Method or by a Linear Program formulation.

\subsection{Subset Sum}

\begin{problem}[Subset Sum]
  \label{problem:assignment}
  ~\\
  \textbf{Given:} A set of $n$ integers, $A = \{a_1, \ldots, a_n\}$ and an integer $k$ \\
  \textbf{Output:} true if there exists a subset $S \subseteq A$ such that
	$$\sum_{s \in S} s = k$$
\end{problem}

\begin{itemize}
  \item Solution: Generate all possible subsets of $A$ and check their sum
  \item Backtracking solution: use the same algorithm as Knapsack; backtrack if sum exceeds $k$
  \item Variation: does there exist a partitioning into two sets whose sums are equal?
\end{itemize}

  \begin{algorithm}[H]
    \Input{A set of $n$ integers, $A = \{a_1, \ldots, a_n\}$, an integer $k$, a partial solution $S = \{a_\ell, \ldots, a_j\} \subseteq A$}
    \Output{A subset $S' \subseteq A$ whose elements sum to $k$ if one exists; nil otherwise}
    \If{$sum(S) = k$}{
      return $S$ \;
    }
    \If{$sum(S) > k$}{
      return nil \;
    }
    \For{$i=j+1, \ldots n$}{
      $T \leftarrow \textsc{SubsetSum}(S \cup \{a_i\}, k)$ \;
      \If{$T \neq nil$}{
        return $T$ \;
      }
    }
    \caption{Brute Force Subset Sum}
    \label{algo:subsetSuml}
  \end{algorithm}


\clearpage
\section{Exercises}

\begin{exercise}
Implement the brute-force solution for the Hamiltonian Path problem in the
high-level programming language of your choice.
\end{exercise}
	
\begin{exercise}
Implement the brute-force solution for the 0-1 Knapsack problem in the
high-level programming language of your choice.
\end{exercise}
	
\begin{exercise}
Consider the following \emph{frequent itemset problem}: we are 
	given a set of \emph{items} $A = \{a_1, a_2, \ldots, a_n\}$ and a set of 
	\emph{baskets} $B_1, B_2, \ldots, B_m$ such that each basket is a subset 
	containing a certain number of items, $B_i \subseteq A$.  Given a \emph{support}
	$s$, \emph{frequent itemsets} are subsets $S \subset A$ such that $S$ is
	a subset of $t$ baskets (that is, the items in $S$ \emph{all} appear in at least
	$s$ baskets).  

	For example, suppose that we have the following baskets:
$$\begin{array}{rcl}
  B_1 & = & \{beer, screws, hammer\} \\
  B_2 & = & \{saw, lugnuts\} \\
  B_3 & = & \{beer, screws, hammer, lugnuts\} \\
  B_4 & = & \{beer, screws, hammer, router\} \\
\end{array}$$

	If our support $s = 2$ then we're looking for all subsets of items that appear
	in at least 2 of the baskets.  In particular:
\begin{itemize}
  \item $\{lugnuts\}$ as it appears in $B_2, B_3$
  \item $\{beer\}$ as it appears in $B_1, B_3, B_4$
  \item $\{hammer\}$ as it appears in $B_1, B_3, B_4$
  \item $\{screws\}$ as it appears in $B_1, B_3, B_4$
  \item $\{beer, screws\}$ as it appears in $B_1, B_3, B_4$
  \item $\{hammer, screws\}$ as it appears in $B_1, B_3, B_4$
  \item $\{hammer, beer\}$ as it appears in $B_1, B_3, B_4$
\end{itemize}

This problem is widely seen in commerce, language analysis, search,  
and financial applications to learn \emph{association rules}.  For example, 
a customer analysis may show that people often buy nails when they buy 
a hammer (the pair has high support), so run a sale on hammers and 
jack up the price of nails!

For this exercise, you will write a program that takes a brute force approach 
(with pruning) and \emph{finds} the support for each possible subset $S \subseteq A$.  
If there is zero support, you will omit it from your output.  Essentially you are solving the
problem for $s = 1$, but we're not only interested in the actual sets, but the
sets \emph{and} the number of baskets that they appear in.  

Write a solution to this problem in the high-level programming language of
your choice.
\end{exercise}
	
