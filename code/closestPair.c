#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<time.h>

typedef struct {
  double x;
  double y;
} Point;

double distance(Point *p1, Point *p2);
double closestPoints(Point *points, int n);
Point * randomPoints(int n, double min, double max);

int reportInterval = 100000000;

int main(int argc, char **argv) {

  int n = 100;
  if(argc > 1) {
    n = atoi(argv[1]);
  }
  srand(time(NULL));
  double min = -1000000.0;
  double max = 1000000.0;
  printf("Creating %d random points between [%f, %f]...\n", n, min, max);
  Point *points = randomPoints(n, min, max);
  printf("Done.\n");

  printf("Starting...\n");
  clock_t begin = clock();

  double minDist = closestPoints(points, n);

  clock_t end = clock();
  double runTime = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("closest pair distance: %f\n", minDist);
  printf("execution time: %f seconds\n", runTime);

}

Point * randomPoints(int n, double min, double max) {

  double range = (max - min);
  Point *pts = (Point*) malloc(sizeof(Point) * n);
  for(int i=0; i<n; i++) {
    pts[i].x = min + (rand() / (RAND_MAX / range));
    pts[i].y = min + (rand() / (RAND_MAX / range));
  }

  return pts;
}


double closestPoints(Point *points, int n) {

  double min = distance(&points[0], &points[1]);;
  int counter = 1;
  for(int i=0; i<n-1; i++) {
    for(int j=i+1; j<n; j++) {
      double dist = distance(&points[i], &points[j]);
      if(dist < min) {
        min = dist;
      }
      counter++;
      if(counter % reportInterval == 0) {
        printf("Compared %d pairs...\n", reportInterval);
      }
    }
  }
  return min;
}

double distance(Point *p1, Point *p2) {
  return sqrt( (p1->x - p2->x) * (p1->x - p2->x) + (p1->y - p2->y) * (p1->y - p2->y));
}
