%!TEX root = ComputerScienceThree.tex

\section{Introduction}

One pitfall of recursive solutions is unnecessarily recomputing solutions.  A classic example is
the Fibonacci sequence.
  $$F_n = F_{n-1} + F_{n-2}, \quad F_0 = 0, F_1 = 1$$
Observing the computation tree of a recursive algorithm to compute a fibonacci number we
realize that many of the computations are repeated an exponential number of times.

One method for avoiding recomputing values is \emph{memoization}: once a value has been
computed it is placed into a table.  Then, instead of recomputing it we check the table; if it
has been computed already, we use the value stored in the table, if not then we go through the
effort of computing it.

Both of these approaches are fundamentally \emph{top-down} solutions.  That is, they start
with the larger problem, then attempt to solve it by computing solutions to sub problems.

\emph{Dynamic Programming} is an algorithmic technique that seeks \emph{bottom-up} solutions.
Rather than dividing the problem up into sub-problems, dynamic programming attempts to solve
the sub-problems \emph{first}, then uses them to solve the larger problem.

This approach typically works by defining a \emph{tableau} (or several) and filling them out
using previously computed values in the tableau.  In general, recursion is avoided and the algorithm
is achieved with just a few loops.

General approach (FAST method): \url{https://blog.pramp.com/how-to-solve-any-dynamic-programming-problem-603b6fbbd771}

\subsection{Optimal Substructure Property}

In general, for a problem to have a dynamic programming 
solution, it must posses the \emph{optimal substructure property}.  
This is a formalization of the idea that the optimal solution to a 
problem can be formulated by (efficiently) finding the optimal 
solution to each of its sub-problems.  This property is also integral 
to greedy algorithmic solutions.  Obviously, not all problems poses 
this property.

\section{Binomial Coefficients}

Recall the choose function:
$${n \choose k} = \frac{n!}{(n-k)!k!}$$
which is usually referred to as a binomial coefficient as it represents the $k$-th coefficient in
the expansion of a binomial:
$$(x + y)^n = {n \choose 0} x^n + \cdots + {n \choose k} x^{n-k}y^k + \cdots {n \choose n} y^n$$
Pascal's identity allows one to express binomial coefficients as the sum of other binomial coefficients:
$${n \choose k} = {n-1 \choose k-1} + {n-1 \choose k}$$
with
$${n \choose 0} = {n \choose n} = 1$$
as base cases to the recursive definition.

However, such a solution turns out to be exponential as many of the sub-solutions are recomputed many
times.  To illustrate, consider Code Snippet \ref{code:recBinomial}.  A recent run of this program on
the cse server to compute \mintinline{c}{binomial(38,12)} resulted in $1,709,984,303$ recursive calls and took
about $20$ seconds.  In contrast, using memoization to avoid repeated computations resulted in only 573 function calls
and took less than 1ms.

\begin{listing}
\begin{minted}{c}
long binomial(int n, int k) {

  if(k < 0 || n < 0) {
    return -1;
  } else if(k == 0 || n == k) {
    return 1;
  } else {
    return binomial(n-1, k-1) + binomial(n-1, k);
  }
}
\end{minted}
\caption{Recursive Binomial Computation}
\label{code:recBinomial}
\end{listing}

Memoization is still a top-down solution that utilizes recursion.  Dynamic programming is a bottom-up
solution that does not make function calls, but rather fills out a tableau of values.

In the case of binomial coefficients, we define a $(n+1) \times (k+1)$ sized table (see Table \ref{table:binomialTableau}).

\input{figures/tabBinomial}

The tableau is filled out top-to-bottom, left-to-right with the trivial base cases
pre-filled.

  \begin{algorithm}[H]
    \Input{Integers, $n, k, 0\leq k\leq n$}
    \Output{The binomial coefficient ${n \choose k}$}
    \For{$i =0, \ldots, n$}{
      \For{$j =0, \ldots, \min{i,k}$}{
        \eIf{$j = 0 \vee j=k$}{
          $C_{i, j} \leftarrow 1$ \;
        }{
          $C_{i,j} \leftarrow C_{i-1,j-1} + C_{i-1,j}$ \;
        }
      }
    }
    output $C_{n,k}$ \;
    \caption{Binomial Coefficient -- Dynamic Programming Solution}
    \label{algo:dynamicProgBinomial}
  \end{algorithm}

Analysis: clearly the elementary operation is addition and in total, the
algorithm is $\Theta(nk)$.  This matches the complexity of a memoization approach, but
avoids any additional function calls.

\section{Optimal Binary Search Trees}
\index{Optimal Binary Search Trees}
\index{Binary Search Trees!Optimal}

We've considered Binary Search Trees and Balanced BSTs.  Consider the following
variation: suppose we know in advance the keys to be searched along with a
known (or estimated) probability distribution of searches on these keys.

We don't necessarily want a balanced binary search tree, rather we want a BST
that will minimize the overall expected (average) number of key comparisons.  Intuitively,
keys with higher probabilities should be shallower in the tree while those with lower
probabilities should be deeper.  Specifically, average number of comparisons made
would be:
  $$\sum_{i=1}^n h(k_i)\cdot p(k_i)$$
where $h(k_i)$ is the level in which the key $k_i$ lies and $p(k_i)$ is the probability
of searching for $k_i$.  Our goal will be to compute the \emph{Optimal Binary Search
Tree} (OBST).

As an example, consider the key distribution in Table \ref{table:obstKeys} and the 
trees in Figure \ref{fig:obstExample}.  There are several valid BSTs, 
but the expected number of comparisons given the key probabilities are different.

\input{figures/figOBSTExample}

The number of possible BSTs with $n$ keys corresponds to the Catalan numbers.

\begin{definition}
  The \emph{Catalan} numbers are a sequence of natural numbers defined by
  $$C_n = \frac{1}{n+1} {2n \choose n} = \frac{(2n)!}{(n+1)!n!}$$
\end{definition}

\begin{itemize}
  \item The first few numbers in the sequence: $1, 1, 2, 5, 14, 42, 132, 429, \ldots, $
  \item Corresponds to the number of valid, balanced parenthesization of operations:
	$$((ab)c)d, (a(bc))d, (ab)(cd), a((bc)d), a(b(cd))$$
  \item Corresponds to the number of ``full'' (every node has 0 or 2 children) 
    binary trees with $n+1$ leaves.  This is the same a binary tree with $n$ 
    key nodes (which will necessarily have $n+1$ \emph{sentinel} leaves).
  \item \emph{Many} other interpretations
  \item They have an exponential growth:
	$$C_n \in O\left(\frac{4^n}{n^{1.5}}\right)$$
\end{itemize}

OBSTs are not ideal for applications that perform a lot of
insertion and deletion operations.  The introduction of a new key or the 
elimination of a key will necessarily change the probability 
distribution and a new OBST may have to be generated from 
scratch.  The use cases are for more static, indexed collections
that do not change frequently.

For a dynamic programming solution (due to Knuth \cite{Knuth71}), 
we need a recurrence.  Let $C_{i, j}$ be defined as the smallest 
average number of comparisons made in a successful search in 
a binary tree $T_{i,j}$ involving keys $k_i$ through $k_j$ for 
$1 \leq i, j \leq n$.  Ultimately, we are interested in finding 
$C_{1, n}$---the optimal BST involving all keys.

To define a recurrence, we need to consider which key should be 
the root of the intermediate tree $T_{i,j}$, say $k_l$.  Such a tree 
will have $k_l$ as the root and a left subtree, $T_{i,l-1}$ containing 
keys $k_i, \ldots k_{l-1}$ optimally arranged and the right-sub-tree, 
$T_{l+1,j}$ containing keys $k_{l+1}, \ldots, j$.

$$C_{i,j} = \min_{i\leq \ell \leq j} \big\{ C_{i, \ell-1} + C_{\ell+1, j} \big\} + \sum_{s=i}^j p(k_s)$$
for $1 \leq i \leq j \leq n$.  Note that the sum is invariant over all 
values of $\ell$: it represents the fact that building the tree necessarily 
adds 1 comparison to the depth of all the keys.  A visualization of the 
split can be found in Figure \ref{fig:obstSplit}.

\input{figures/figOBSTSplit}

Some obvious corner cases:
 \begin{itemize}
   \item $C_{i, i-1} = 0$ for $1 \leq i \leq n+1$ since no comparisons
      are made in an empty tree.
   \item $C_{i, i} = p(k_i)$ for $1 \leq i \leq n$ since an optimal tree
      of size 1 is the item itself.
 \end{itemize}

With these values, we can define an $(n+1) \times (n+1)$ tableau (see Table \ref{table:obstTableau}).

    \begin{table}
    \centering

\begin{tabular}{|l||*{5}{c|}}
\hline
\backslashbox{$i$}{$j$} & 0 & 1 & 2 & $\cdots$ & $n$ \\
    \hline
    \hline
    1 & 0 & $p(k_1)$ & ~ & ~ & $C_{1, n}$ \\
    \hline
    2 & 0 & 0        & $p(k_2)$ & ~ & ~ \\
    \hline
    $\vdots$ & ~ & ~ & ~ & $\ddots$ & ~  \\
    \hline
    $n$ & 0 & 0 & $\cdots$ & 0 & $p(k_n)$ \\
    \hline
    $n+1$ & 0 & 0 & $\cdots$ & 0 & 0\\
    \hline
    \end{tabular}
    \caption{Optimal Binary Search Tree Tableau}
    \label{table:obstTableau}
    \end{table}

Each entry in the tableau requires previous values in the same row
to the left and in the same column below as depicted in Figure 
\ref{fig:obstVisual}.  Thus, the tableau is filled out along the diagonals 
from top-left to bottom-right.  The first diagonal is initialized to zero 
and the second with each of the respective probabilities of each key.  
Then each successive table entry is calculated according to our recurrence.

\input{figures/figOBSTTableau}

The final value $C_{1, n}$ is what we seek.  However, this gives us the
average number of key comparisons (minimized), not the actual tree.
To build the tree, we also need to maintain a \emph{root table} that
keeps track of the values of $\ell$ for which we choose the minimum (thus
$k_\ell$ is the root and we can build top down). 

A stack-based algorithm for constructing the OBST is presented in Algorithm \ref{algo:obstTreeConstruction}.

  \begin{algorithm}[H]
    \Input{A set of keys $k_1, \ldots, k_n$ and a probability distribution $p$ on the keys}
    \Output{An optimal Binary Search Tree}
    \For{$i = 1, \ldots, n$}{
      $C_{i,i-1} \leftarrow 0$ \;
      $C_{i,i} \leftarrow p(k_i)$ \;
      $R_{i,i} \leftarrow i$ \;
    }
    $C_{n+1,n} \leftarrow 0$ \;
    \For{$d =1, \ldots, (n-1)$}{
      \For{$i =1, \ldots, (n-d)$}{
        $j \leftarrow i + d$ \;
        $min \leftarrow \infty$ \;
        \For{$\ell=i,\ldots, j$}{
          $q \leftarrow C_{i,\ell-1} + C_{\ell+1,j}$ \;
          \If{$q < min$}{
            $min \leftarrow q$ \;
            $R_{i,j} \leftarrow \ell$ \;
          }
        }
        $C_{i,j} \leftarrow min + \sum_{s=i}^{j} p(k_s)$ \;
      }
    }
    output $C_{1,n}, R$ \;
    \caption{Optimal Binary Search Tree}
    \label{algo:obst}
  \end{algorithm}

\newpage 

  \begin{algorithm}[H]
    \Input{A set of keys $k_1, \ldots, k_n$ and root Table $R$}
    \Output{The root node of the OBST}
    $root \leftarrow$ new root node \;
    $r.key \leftarrow R_{1, n}$ \;
    $S \leftarrow $ empty stack \;
    $S.push (r, 1, n)$ \Comment{In general, we push a node $u$, and indices $i, j$}
    \While{$S$ is not empty}{
      $(u, i, j) \leftarrow S.pop$ \;
      $k \leftarrow R_{i, j}$ \Comment{this is the key corresponding to $u$}
      \If{$k < j$}{
        \Comment{create the right child and push it}
        $v \leftarrow $ new node \;
        $v.key \leftarrow R_{k+1, j}$ \;
        $u.rightChild \leftarrow v$ \;
        $S.push (v, k+1, j)$ \;    
      }
      \If{$i < k$}{
        \Comment{create the left child and push it}
        $v \leftarrow $ new node \;
        $v.key \leftarrow R_{i, k-1}$ \;
        $u.leftChild \leftarrow v$ \;
        $S.push (v, i, k-1)$ \;    
      }
    }
    output $root$ \;
    \caption{OBST Tree Construction}
    \label{algo:obstTreeConstruction}
  \end{algorithm}

\subsection{Example}

\begin{table}
\centering
\begin{tabular}{cc}
Key & Probability \\
\hline\hline
A & $.213$ \\
B & $.020$ \\
C & $.547$ \\
D & $.100$ \\
E & $.120$ \\
\end{tabular}
\caption{Optimal Binary Search Tree Example Input}
\label{table:obstExampleInput}
\end{table}

The full tableaus can be found in Table \ref{table:obstExampleResults}.  As an example computation:

$$C_{1,2} = \min_{1\leq \ell\leq 2} \left\{ \begin{array}{l}
\ell=1: C_{1, 0} + C_{2, 2} + \sum_{s=1}^2 p(k_s) = 0 + .02 + (2.13 + .02) = .253 \\
\ell=2: C_{1, 1} + C_{3, 2} + \sum_{s=1}^2 p(k_s) = 0.213 + .02 + (2.13 + .02) = .446 \\
\end{array}\right.$$
These two options correspond to splitting the sub-tree involving keys $A, B$ at $A, B$ respectively.  That
is, $A$ as the root with $B$ as its right-child \emph{or} $B$ as the root with $A$ as its left-child.  The
values indicate that $k=1$ is the better option.

$$C_{2,5} = \min_{2\leq \ell\leq 5} \left\{ \begin{array}{l}
\ell=2: C_{2, 1} + C_{3, 5} + \sum_{s=2}^5 p(k_s) = 1.874 \\
\ell=3: C_{2, 2} + C_{4, 5} + \sum_{s=2}^5 p(k_s) = 1.127 \\
\ell=4: C_{2, 3} + C_{5, 5} + \sum_{s=2}^5 p(k_s) = 1.494 \\
\ell=5: C_{2, 4} + C_{6, 5} + \sum_{s=2}^5 p(k_s) = 1.573 \\
\end{array}\right.$$

\begin{table}
\centering
\subfigure[Cost tableau for OBST]{
\begin{tabular}{|l||*{6}{r|}}
\hline
\backslashbox{$i$}{$j$} & 0 & 1 & 2 & 3 & 4 & 5 \\
    \hline\hline
    1 & 0 & 0.213 & 0.253 & 1.033 & 1.233 & 1.573 \\
    \hline
    2 & ~ & 0 & 0.020 & 0.587 & 0.787 & 1.127 \\
    \hline
    3 & ~ & ~ & 0 & 0.547 & 0.747 & 1.087 \\
    \hline
    4 & ~ & ~ & ~ & 0 & 0.100 & 0.320 \\
    \hline
    5 & ~ & ~ & ~ & ~ & 0 & 0.120 \\
    \hline
    6 & ~ & ~ & ~ & ~ & ~ & 0 \\
    \hline
\end{tabular}
}\subfigure[Root array indicating which keys resulted in the optimal split.]{
\begin{tabular}{|l||*{5}{r|}}
\hline
\backslashbox{$i$}{$j$} & 1 & 2 & 3 & 4 & 5 \\
    \hline\hline
    1 & 1 & 1 & 3 & 3 & 3 \\
    \hline
    2 & ~ & 2 & 3 & 3 & 3 \\
    \hline
    3 & ~ & ~ & 3 & 3 & 3 \\
    \hline
    4 & ~ & ~ & ~ & 4 & 5 \\
    \hline
    5 & ~ & ~ & ~ & ~ & 5 \\
    \hline
\end{tabular}
}
\caption{Tableaus resulting from the Optimal Binary Search Tree example}
\label{table:obstExampleResults}
\end{table}

The final result can be seen in Figure \ref{fig:obstResult}.  The 
expected number of key comparisons for any search is $1.573$.  
Though this particular example resulted in a balanced
BST, in general, OBSTs need not be balanced.

\input{figures/figOBSTResult}

\section{Dynamic Knapsack}

Recall the 0-1 Knapsack Problem (see Problem \ref{problem:zeroOneKnapsack}).  This
problem lends itself to an ``efficient'' dynamic programming solution.

Let $V_{i,j}$ be the optimal solution (a subset of items) involving
the first $i$ objects subject to an intermediate weight constraint
$j$.  Clearly, $1 \leq i \leq n$ and $1 \leq j \leq W$.

For a given $V_{i,j}$ we can divide all the possible subsets of the
first $i$ items that fit a knapsack capacity of $j$ into two groups:
those that include item $i$ and those that do not.

\begin{itemize}
  \item Among subsets that \emph{do not} include the $i$-th element,
      the value of the optimal subset is $V_{i-1, j}$.
  \item Among the subsets that \emph{do} include the $i$-th element,
      the optimal subset will be made up of the $i$-th item and the optimal
      subset of the first $i-1$ items that fit into a knapsack of capacity
      $j-w_i$.  This is exactly
      $$v_i + V_{i-1, j-w_i}$$.
\end{itemize}

We are interested in maximizing our total value, so we take the max of these two
solutions if feasible.
$$V_{i, j} = \left\{\begin{array}{ll}
  \max\big\{ V_{i-1, j}, \,\, v_i + V_{i-1, j-w_i} \big\} & \textrm{if } j - w_i \geq 0\\
    V_{i-1, j} & \textrm{if } j - w_i < 0
    \end{array}\right.$$

In addition, we define the initial conditions
\begin{itemize}
  \item $V_{0, j} = 0$ for $j \geq 0$ (taking no items has no value)
  \item $V_{i,0} = 0$ for $i \geq 0$ (no capacity means we can take no items)
\end{itemize}

The tableau that we fill out will be a $(n+1) \times (W+1)$ (rows,
columns) sized table numbered $0 \ldots n$ and $0 \ldots W$ respectively.

\begin{table}
\centering

\begin{tabular}{|l||*{7}{c|}}
\hline
\backslashbox{$i$}{$j$} & 0 & $\cdots$ & $j - w_i$ & $\cdots$ & ~ & $\cdots$ & $W$ \\
    \hline\hline
    0 & 0 & $\cdots$ & 0 & $\cdots$ & 0 & $\cdots$ & 0\\
    \hline
    1 & 0 & $\cdots$ & ~ & $\cdots$ & ~ & $\cdots$ & ~\\
    \hline
    $\vdots$ \\
    \hline
    $i-1$ & 0 & $\cdots$ & $V_{i-1, j - w_i}$ & $\cdots$ & $V_{i-1, j}$ & $\cdots$ & $~$\\
    \hline
    $i$ & 0 & $\cdots$ & ~ & $\cdots$ & $V_{i, j}$ & $\cdots$ & $~$\\
    \hline
    $\vdots$ \\
    \hline
    $n$ & 0 & $\cdots$ & ~ & $\cdots$ & $V_{i, j}$ & $\cdots$ & $V_{n, W}$\\
    \hline
    \end{tabular}
\caption[Tableau for Dynamic Knapsack]{Tableau for Dynamic Knapsack.  The
value of an entry $V_{i,j}$ depends on the values in the previous row and columns.}
\label{table:dynamicKnapsackTableau}
\end{table}

The tableau can be filled out as follows.  For each entry, we can take
the maximum of:
\begin{itemize}
  \item the entry in the previous row of the same column and
  \item the sum of $v_i$ and the entry in the previous row and $w_i$ columns to
	the left.
\end{itemize}

This enables us to calculate row-by-row (left to right) or column-by-column (top to bottom).

We can also build the optimal solution using the same table by working backwards.
For each intermediate capacity $w$, item $i$ is in the optimal subset
if $V_{i, w} \neq V_{i-1, w}$.  If this is the case, we can update the intermediate
capacity, $w - w_i$ and look at the corresponding column to continue.

Visually, we start with entry $V_{n, W}$.  We then scan upwards in this column until
the table value changes.  A change from row $i$ to row $i-1$ corresponds to taking
item $i$ with weight $w_i$.  We then jump left in the table on row $i-1$ to column $j-w_i$
(where $j$ is the column we started out) and repeat the process until we have reached
the border of the tableau.

  \begin{algorithm}[H]
    \Input{Completed tableau $V$ of a Dynamic Programming 0-1 knapsack solution}
    \Output{The optimal knapsack $S$}
    $S \leftarrow \emptyset$ \;
    $i \leftarrow n$ \;
    $j \leftarrow W$ \;
    \While{$i \geq 1 \wedge j \geq 1$}{
      \While{$i \geq 1 \wedge V_{i,j} = V_{i-1,j}$}{
        $i\leftarrow (i-1)$ \;
      }
      $S \leftarrow S \cup \{a_i\}$ \;
      $j \leftarrow (j - w_i)$ \;
      $i \leftarrow (i-1)$ \;
    }
    output $S$ \;
    \caption{0-1 Knapsack Generator}
    \label{algo:knapsackGenerator}
  \end{algorithm}

\subsection{Example}

\begin{table}
\centering
\begin{tabular}{ccc}
Item & Weight & Value \\
\hline\hline
$a_1$ & 5 & 10 \\
$a_2$ & 2 & 5 \\
$a_3$ & 4 & 8 \\
$a_4$ & 2 & 7 \\
$a_5$ & 3 & 7 \\
\end{tabular}
\caption{Example input for 0-1 Knapsack}
\label{table:dynamicKnapsackInput}
\end{table}

\begin{table}
\centering
\begin{tabular}{|l||*{8}{c|}}
\hline
\backslashbox{$i$}{$j$} & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
\hline\hline
0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
\hline
1 & 0 & 0 & 0 & 0 & 0 & 10 & 10 & 10 \\
\hline
2 & 0 & 0 & 5 & 5 & 5 & 10 & 10 & 15 \\
\hline
3 & 0 & 0 & 5 & 5 & 8 & 10 & 13 & 15 \\
\hline
4 & 0 & 0 & 7 & 7 & 12 & 12 & 15 & 17 \\
\hline
5 & 0 & 0 & 7 & 7 & 12 & 14 & 15 & 19 \\
\hline
\end{tabular}
\caption{Resulting tableau for the 0-1 Knapsack Example}
\label{table:dynamicKnapsackResult}
\end{table}

The resulting tableau can be found in figure \ref{table:dynamicKnapsackResult}.  The corresponding
optimal knapsack would be $\{a_2, a_4, a_5\}$; the backtracking is illustrated in Figure
\ref{fig:dynamicKnapsackBacktrack}.

\input{figures/figDynamicKnapsackBacktrack}

\subsection{Analysis}

Clearly, the running time is equivalent to the number of entries that we compute, $\Theta(nW)$.
Unfortunately this fails to give a complete picture.  Recall that $W$ is part of the input, thus the
input \emph{size} is $\log{W}$.  If $W \in O(2^n)$ for example, this is clearly not polynomial.

Algorithms with analysis like this are called \emph{pseudopolynomial} (or more specifically,
\emph{pseudolinear} in this case).  This is because another parameter of the input ``hides''
the true complexity of the algorithm.  If we considered $W$ to be constant or even $W\in O(n^k)$
then the algorithm runs in polynomial time.  In general, such restrictions are not reasonable and
the problem remains $\NP$-complete.

\section{Coin Change Problem}

    The Coin Change problem is the problem of giving the minimum number
    of coins adding up to a total $L$ in change using a given set of
    denominations $\{c_1, \ldots, c_n\}$.  For certain denominations
    (like US currency, how fortunate!), a simple greedy strategy works.

    However, for other denominations, this approach doesn't necessarily
    work.  A dynamic programming solution, however, guarantees an
    optimal solution.  Optimal here means that we are \emph{minimizing}
    the number of coins used to make $L$ change.

    Let $C_{i,j}$ be defined as the minimum number of coins from denominations $\{c_1,
    \ldots , c_i\}$ that add up to $j$ for $1\leq i \leq n$ and $0\leq
    j \leq L$.  To build this array, we require two base cases.

\begin{itemize}
  \item $C_{i,0} = 0$ for all $i$ (no coins needed for a total of zero)
  \item $C_{0,j} = \infty$ for $j\neq 0$ (change is not possible with no coins)
\end{itemize}

Now, we can define a recurrence based on the intuitive subproblem split:

$$C_{i, j} = \left\{\begin{array}{ll}
    C_{i-1, j} & \textrm{ for } j<c_i\\
    \min \{ C_{i-1, j}, \quad C_{i, j-c_i}+1 \} & \textrm{ for } j\geq c_i\\
\end{array}\right.$$

The corresponding tableau can be found in Table \ref{table:coinChangeTableau}.

\begin{table}
\centering
\begin{tabular}{|l||*{5}{c|}}
\hline
\backslashbox{$i$}{$j$} & 0 & 1 & 2 & $\cdots$ & $C$ \\
\hline\hline
 0 & $\infty$ & $\infty$& $\infty$& $\cdots$ & $\infty$\\
\hline
 1 & 0 & $\ddots$ & ~ & ~ & ~ \\
\hline
 2 & 0 & ~ & ~ & ~ & ~ \\
\hline
 $\vdots$ & $\vdots$ & ~ & ~ & ~ & ~ \\
 \hline
 $n$ & 0 & ~ & ~ & ~ & ~ \\
 \hline
\end{tabular}
\caption{Tableau for the Coin Change Problem}
\label{table:coinChangeTableau}
\end{table}

The tableau is filled row-by-row (left-to-right) from top-to-bottom.  The
final solution (the optimal number of coins) is found in entry $C_{n,L}$.

\subsection{Example}

Suppose that we have a set of four coin denominations (see Table \ref{table:coinChangeInput}).

\begin{table}[h]
\centering
\begin{tabular}{cc}
Denomination & Value \\
\hline\hline
$c_1$ & 5 \\
$c_2$ & 2 \\
$c_3$ & 4 \\
$c_4$ & 1 \\
\end{tabular}
\caption{Coin Change Example Input}
\label{table:coinChangeInput}
\end{table}

The resulting tableau can be found in Table \ref{table:coinChangeResult}.

\begin{table}[h]
\centering
\begin{tabular}{|l|l||*{10}{c|}}
\hline
$c_i$ & \backslashbox{$i$}{$j$} & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8\\
\hline\hline
- & 0 & - & $\infty$ & $\infty$& $\infty$& $\infty$ & $\infty$ & $\infty$ & $\infty$ & $\infty$\\
\hline
5 & 1 & 0 & $\infty$ & $\infty$ & $\infty$ & $\infty$ & 1 & $\infty$ & $\infty$ & $\infty$\\
\hline
2 & 2 & 0 & $\infty$ & 1 & $\infty$ & 2 & 1 & 3 & 2 & 4\\
\hline
4 & 3 & 0 & $\infty$ & 1 & $\infty$ & 1 & 1 & 2 & 2 & 2\\
\hline
1 & 4 & 0 & 1 & 1 & 2 & 1 & 1 & 2 & 2 & \textbf{2}\\
\hline
\end{tabular}
\caption{Coin Change Example Results}
\label{table:coinChangeResult}
\end{table}


\section{Matrix Chain Multiplication}
\index{Matrix Chain Multiplication}

Suppose we have three matrices, $A, B, C$ of dimensions $(5 \times 10), (10 \times 15), (15 \times 10)$
respectively.  If we were to perform the multiplication $ABC$, we could do it one of two ways; multiply $AB$ first
then by $C$ or $BC$ first then by $A$.  That is, either
$$(AB)C$$
or
$$A(BC)$$
Using straightforward matrix multiplication, the first way would result in
$$(5 \cdot 10 \cdot 15) + (5 \cdot 15 \cdot 10) = 1,500$$
multiplications while the second would result in
$$(5 \cdot 10 \cdot 10) + (10 \cdot 15 \cdot 10) = 2,000$$
multiplications.

In general, suppose that we are given a \emph{chain} of matrices $A_1, \ldots A_n$
and we wanted to multiply them all but minimize the total number of multiplications.
This is the \emph{matrix chain multiplication problem}.  More generally, what parenthesization
of the associative operations minimizes the overall number of multiplication made
to compute the product?

Clearly, each product, $A_iA_{i+1}$ has to be a valid operation.  Further, we will
assume that each matrix has different dimensions ($d_1, \ldots, d_{n+1}$)---if they were all square there
would be no point in finding the optimal order (they would all be the same).

For this problem we define a (lower-triangular) table of size
$n \times n$.  Each entry represents the minimum number of multiplications
it takes to evaluate the matrices from $A_i\ldots A_j$.  The tableau
can be filled in using the following recurrence relation:

    $$C(i, j) = \left\{\begin{array}{ll}
    0 & \textrm{if $i=j$}\\
    {\displaystyle \min_{i\leq k < j} \left\{C(i, k) + C(k+1, j) + d_{i-1}d_kd_j\right\}} & \mathrm{otherwise}\\
    \end{array}\right.$$

The order in which the tableau is filled in begins with the
base cases, down the diagonal where $i=j$. Depending on how you
orient your tableau, you will then work downward in a diagonal
fashion, see Table \ref{fig:matrixChainTableau}.

\begin{table}
\centering
\begin{tabular}{|l||*{4}{c|}}\hline
\backslashbox{$i$}{$j$} & 1 & 2 & $\cdots$ & $n$ \\
\hline\hline
1 & 0 & ~ & ~ & ~\\
\hline
2 & $\phi$ & 0& ~ & ~ \\
\hline
$\vdots$ & $\vdots$ & ~ & $\ddots$ & \\
\hline
$n$ & $\phi$ & $\phi$ & $\cdots$ & 0\\
\hline
\end{tabular}
\caption{Matrix Chain Multiplication Tableau.}
\label{fig:matrixChainTableau}
\end{table}

In addition, at each calculation, we fill in an $S$-array that keeps track of which
value of $k$ was chosen for each entry.  This value can be used to construct the
optimal solution.  The $S$-array ranges $2 \leq i \leq n$ and $1 \leq j \leq n-1$; see
Table \ref{fig:matrixChainSTableau}.

\begin{table}
\centering
\begin{tabular}{|l||*{4}{c|}}\hline
\backslashbox{$i$}{$j$} & 1 & 2 & $\cdots$ & $n-1$ \\
\hline\hline
2 & 0 & ~ & ~ & ~\\
\hline
3 & $\phi$ & 0& ~ & ~ \\
\hline
$\vdots$ & $\vdots$ & ~ & $\ddots$ & ~\\
\hline
$n$ & $\phi$ & $\phi$ & $\cdots$ & 0\\
\hline
\end{tabular}
\caption{Matrix Chain Multiplication $S$-array.}
\label{fig:matrixChainSTableau}
\end{table}

  \begin{algorithm}[H]
    \Input{$n$ matrices, $A_1, \ldots, A_n$ with dimensions $d_0, \ldots, d_{n+1}$}
    \Output{The number of multiplications in the optimal multiplication order}

    \For{$i=1, \ldots, n$}{
      $C_{i,j} \leftarrow 0$ \;
      $S_{i+1, j} \leftarrow i$ \;
    }
    \For{$l=2, \ldots, n$}{
      \For{$i=1, \ldots, n-l+1$}{
        $j \leftarrow i+l - 1$ \;
        $C_{i,j} \leftarrow \infty$ \;
        \For{$k = i, \ldots, (j-1)$}{
          $q \leftarrow C_{i, k} + C_{k +1, j} + d_{i-1}d_kd_j$ \;
          \If{$q < C_{i,j}$}{
            $C_{i,j} \leftarrow q$ \;
            $S_{i,j} \leftarrow k$ \;
          }
        }
      }
    }
    output $C_{1,n}, S$ \;
    \caption{Optimal Matrix Chain Multiplication}
    \label{algo:matrixChain}
  \end{algorithm}

\subsection{Example}

    Consider the following 6 matrices:
\begin{table}
\centering
\begin{tabular}{cc}
Matrix & Dimensions \\
\hline\hline
$A_1$ & $30 \times 35$\\
$A_2$ & $35 \times 15$\\
$A_3$ & $15 \times 5$\\
$A_4$ & $5 \times 10$\\
$A_5$ & $10 \times 20$\\
$A_6$ & $20 \times 25$\\
\hline
\end{tabular}
\caption{Example Matrix Chain Multiplication Input}
\label{table:matrixChainInput}
\end{table}

The final tableau can be found in Table \ref{table:MatrixChainResult}.

\begin{table}
\centering
\begin{tabular}{|l||*{6}{r|}}\hline
\backslashbox{$i$}{$j$} & 1 & 2 & 3 & 4 & 5 & 6 \\
\hline\hline
1 & 0 & 15,750 & 7,875 & 9,375 & 11,875 & 15,125\\
\hline
2 & $\phi$ & 0& 2,625 & 4,375 &  7,125 & 10,500\\
\hline
3 & $\phi$ & $\phi$ & 0 & 750 &  2,500 & 5,375\\
\hline
4 & $\phi$ & $\phi$ & $\phi$ & 0 & 1,000 & 3,500\\
\hline
5 & $\phi$ & $\phi$ & $\phi$ & $\phi$ & 0 & 5,000\\
\hline
6 & $\phi$ & $\phi$ & $\phi$ & $\phi$ & $\phi$ & 0\\
\hline
\end{tabular}
\caption{Final Tableau for the Matrix Chain Example}
\label{table:MatrixChainResult}
\end{table}

For an example of the computation, consider the index at $C_{2,5}:$

$$C_{2,5} = \min_{2\leq k \leq 4} \left\{\begin{array}{ll}
C_{2, 2} + C_{3, 5} + d_1d_2d_5 = 0 + 2500+35\cdot 15\cdot 20 & = 13,000\\
C_{2, 3} + C_{4, 5} + d_1d_3d_5 = 2625 + 1000 + 35\cdot 5\cdot 20 & = 7,125\\
C_{2, 4} + C_{5, 5} + d_1d_4d_5 = 4375 + 0 + 35\cdot 10\cdot 20 & = 11,375\\
\end{array}\right.$$

Clearly, the minimum among these is 7,125.  The $k$ value that
gave this result was $3$, consequently we place this value into
our $S$-array, $S_{2,5} = 3$.  The full $S$-array can be found in
Table \ref{table:MatrixChainSResult}.

\begin{table}
\centering
\begin{tabular}{|l||*{5}{r|}}\hline
\backslashbox{$i$}{$j$} & 2 & 3 & 4 & 5 & 6 \\
\hline\hline
1 & 1 & 1 & 3 & 3 & 3\\
\hline
2 & ~ & 2 & 3 & 3 & 3\\
\hline
3 & ~ & ~ & 3 & 3 & 3\\
 \hline
4 & ~ & ~ & ~ & 4 & 5\\
\hline
5 & ~ & ~ & ~ & ~ & 5\\
\hline
\end{tabular}
\caption{Final $S$-array for the Matrix Chain Example}
\label{table:MatrixChainSResult}
\end{table}

Each entry in the $S$-array records the value of $k$ such that the
optimal parenthesization of $A_iA_{i+1}A_{i+2}\ldots A_j$ splits
the product between $A_k$ and $A_{k+1}$.  Thus we know that the
final matrix multiplication in computing $A_{1\ldots n}$ optimally
is $A_{1\ldots s[1,n]}A_{s[1,n]+1\ldots n}$.  Multiplications in
each sub-chain can be calculated recursively; the intuitive base
case is when you only have two matrices to multiply.

From the $S$-array above, the initial split would be at $S_{1,6} = 3$.
The full parenthesization would be:
$$\left((A_1)(A_2A_3)\right)\left((A_4A_5)(A_6)\right)$$


\section{Exercises}

\begin{exercise}
Construct the $C$ tableau, $S$-array and final parenthesization for the following matrices:
\begin{table}[h]
\centering
\begin{tabular}{cc}
Matrix & Dimensions \\
\hline\hline
$A_1$ & $5 \times 10$\\
$A_2$ & $10 \times 3$\\
$A_3$ & $3 \times 12$\\
$A_4$ & $12 \times 5$\\
$A_5$ & $5 \times 50$\\
$A_6$ & $50 \times 6$\\
\end{tabular}
\caption{Matrix Chain Multiplication Exercise}
\label{table:matrixChainExercise}
\end{table}
\end{exercise}

\begin{solution}
Solution: 2010 multiplications, $(A_1A_2)((A_3A_4)(A_5A_6))$
\end{solution}

\begin{exercise}
Gomer thinks he's found a better solution to constructing an Optimal 
Binary Search Tree.  He thinks a greedy strategy would work just as well:
Sort the keys and choose the one with the highest probability as the root
of the tree.  Then, repeat this process with the left/right keys.  Show that
Gomer's strategy will not work by providing an example input that will
not be optimal with his strategy.  
\end{exercise}

\begin{solution}	
Many pare possible, one: $a, b, c$: .34, .33, .33
\end{solution}

\begin{exercise}
Implement the dynamic programming solution to the Optimal Binary
Search Tree problem in the high-level programming language of your choice.
\end{exercise}

\begin{exercise}
Implement the dynamic programming solution to the Matrix Chain
Multiplication problem in the high-level programming language of your choice.
\end{exercise}

\begin{exercise}
Implement the dynamic programming solution to the 0-1 knapsack
problem in the high-level programming language of your choice.
\end{exercise}

\begin{exercise}
Implement the dynamic programming solution to the Coin Change
problem in the high-level programming language of your choice.
\end{exercise}

\begin{exercise}
Consider the following ``game'': $n$ items of values $v_1, v_2, \ldots, v_n$
are placed in a line.  You are allowed to take an item only at the beginning
or the end of the line.  Once you take an item, the second player is allowed
to take an item at the beginning or the end of the line.  You take turns
like this until all items have been taken.  The winner is the player whose 
item values sum to the greater amount.

Prove or disprove: a greedy-choice strategy will work; that is, always take 
the item with the higher value.
\end{exercise}

\begin{exercise}
Recall that a \emph{palindrome} is a string that is the same backwards 
  	and forwards (``abba'' or ``deified'').
	\begin{enumerate}[(a)]
	  \item Write pseudocode that, given a string $s$, determines the longest 
	  	\emph{contiguous} palindromic subsequence in $s$.  Analyze your 
		algorithm.
	  \item A palindromic subsequence may not necessarily be contiguous.  For 
	  	example, 
		  $$BBABCBCAB$$
		has palindromic subsequences ``BBBBB'',  ``BBCBB'' and ``BABCBAB'' 
		among others, but are made from non-contiguous elements.  Devise a 
		dynamic programming solution to find the length of the longest palindromic 
		subsequence of a given string $s$.  In addition to finding the length, 
		devise a scheme to output a palindromic subsequence of maximal 
		length.  Give pseudocode and analyze your solution.  Hint: consider 
		defining a tableau $P$ such that $P_{i, j}$ represents the length of the 
		maximal palindromic subsequence within the substring $s_i, \ldots, s_j$ 
		(where $s = s_1s_2\ldots s_n$).
	  \item Implement your dynamic programming solution in in the high-level 
	  	programming language of your choice.  
	\end{enumerate}
\end{exercise}

\begin{solution}
Note that 
$$P[i, i] = 1$$
as any single character string is a palindrome; moreover $P[i, j] = 0$ if $i > j$.   Then 
$$P[i, j] = \left\{ 
\begin{array}{ll}
P[i+1, j-1] + 2 & \textrm{if } s_i = s_j \\
\max\{P[i+1, j], P[i, j-1]\} & \textrm{otherwise}
\end{array}
\right.$$
The first case is where the two characters on the end of the substring match in which case
you add two (for the two characters) and the optimal solution for the substring \emph{not} including
the two end characters.

The second case is when the two end characters do not match: then you choose the better solution 
between excluding the start character and including the end character versus including the start character and excluding the 
end character.
\end{solution}



