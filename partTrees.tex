%!TEX root = ComputerScienceThree.tex

\section{Introduction}

Motivation: we want a data structure to store elements that offers efficient, arbitrary retrieval (search), insertion, and deletion.

\begin{itemize}
  \item Array-based Lists
  \begin{itemize}
    \item $O(n)$ insertion and deletion
    \item Fast index-based retrieval
    \item Efficient binary search if sorted
  \end{itemize}
  \item Linked Lists
  \begin{itemize}
    \item Efficient, $O(1)$ insert/delete for head/tail
    \item Inefficient, $O(n)$ arbitrary search/insert/delete
    \item Efficient binary search not possible without random access
  \end{itemize}
  \item Stacks and queues are efficient, but are restricted access data structures
  \item Possible alternative: Trees
  \item Trees have the potential to provide $O(\log{n})$ efficiency for all operations
\end{itemize}


\section{Definitions \& Terminology}

\begin{itemize}
  \item A \emph{tree} is an acyclic graph
  \item For our purposes: a \emph{tree} is a collection of \emph{nodes} (that can hold keys, data, etc.) that are connected by \emph{edges}
  \item Trees are also \emph{oriented}: each node has a parent and children
  \item A node with no parents is the \emph{root} of the tree, all child nodes are oriented downward
  \item Nodes not immediately connected can have an ancestor, descendant or cousin relationship
  \item A node with no children is a \emph{leaf}
  \item A tree such that all nodes have at most two children is called a \emph{binary tree}
  \item A binary tree is also oriented horizontally: each node may have a left and/or a right child
  \item Example: see Figure \ref{fig:BinaryTree}
  \item A \emph{path} in a tree is a sequence nodes connected by edges
  \item The \emph{length} of a path in a tree is the number of edges in the path (which equals the number of nodes in the path minus one)
  \item A path is \emph{simple} if it does not traverse nodes more than once (this is the default type of path)
  \item The \emph{depth} of a node $u$ is the length of the (unique) path from the root to $u$
  \item The depth of the root is $0$
  \item The depth of a tree is the maximal depth of any node in the tree (sometimes the term \emph{height} is used)
  \item All nodes of the same depth are considered to be at the same \emph{level}
  \item A binary tree is \emph{complete} (also called \emph{full} or \emph{perfect}) if all nodes are present at all levels 0 up to its depth $d$
  \item A sub-tree rooted at a node $u$ is the tree consisting of all descendants with $u$ oriented as the root
\end{itemize}

\begin{figure}[h]
\centering
\input{figures/treeFig}
\caption{A Binary Tree}
\label{fig:BinaryTree}
\end{figure}

Properties:
\begin{itemize}
  \item In a tree, all nodes are connected by exactly one unique path
  \item The maximum number of nodes at any level $k$ is $2^k$
  \item Thus, the maximum number of nodes $n$ for any binary tree of depth $d$ is:
	$$n = 2^0 + 2^1 + 2^2 + \cdots + 2^{d-1} + 2^d = \sum_{k=0}^d 2^k = 2^{d+1} - 1$$
  \item Given a \emph{full binary tree} with $n$ nodes in it has depth:
	$$d =  \log{(n+1)} - 1$$
  \item That is, $d = O(\log{n})$
\end{itemize}

Motivation: if we can create a tree-based data structure with operations proportional to its depth, then we
could potentially have a data structure that allows retrieval/search, insertion, and deletion in $O(\log{n})$-time.

\section{Tree Traversal}

\begin{itemize}
  \item Given a tree, we need a way to enumerate elements in a tree
  \item Many algorithms exist to iterate over the elements in a tree
  \item We'll look at several variations on a depth-first-search
\end{itemize}

\subsection{Preorder Traversal}

\begin{itemize}
  \item A preorder traversal strategy visits nodes in the following order: root; left-sub-tree; right-sub-tree
  \item An example traversal on the tree in Figure \ref{fig:BinaryTree}:
	$$a, b, d, g, l, m, r, h, n, e, i, o, c, f, j, p, q, k$$
  \item Applications:
  \begin{itemize}
    \item Building a tree, traversing a tree, copying a tree, etc.
    \item Efficient stack-based implementation
    \item Used in prefix notation (polish notation); used in languages such as Lisp/Scheme
  \end{itemize}
\end{itemize}

Simulation of a stack-based preorder traversal of the binary tree in Figure \ref{fig:BinaryTree}.

\begin{center}
{\footnotesize
\input{figures/stackTraversalPreorder}
}
\end{center}


\subsection{Inorder Traversal}

\begin{itemize}
  \item An inorder traversal strategy visits nodes in the following order: left-sub-tree; root; right-sub-tree
  \item An example traversal on the tree in Figure \ref{fig:BinaryTree}:
	$$l, g, r, m, d, h, n, b, e, o, i, a, c, p, j, q, f, k$$
  \item Applications:
  \begin{itemize}
    \item Enumerating elements in order in a binary search tree
    \item Expression trees
  \end{itemize}
\end{itemize}

Simulation of a stack-based inorder traversal of the binary tree in Figure \ref{fig:BinaryTree}.

\begin{center}
{\footnotesize
\input{figures/stackTraversalInorder}
}
\end{center}

\subsection{Postorder Traversal}

\begin{itemize}
  \item A postorder traversal strategy visits nodes in the following order: left-sub-tree; right-sub-tree; root
  \item An example traversal on the tree in Figure \ref{fig:BinaryTree}:
	$$l, r, m, g, n, h, d, o, i, e, b, p, q, j, k, f, c, a$$
  \item Applications:
  \begin{itemize}
    \item Topological sorting
    \item Destroying a tree when manual memory management is necessary (roots are the last thing that get cleaned up)
    \item Reverse polish notation (operand-operand-operator, unambiguous, used in old HP calculators)
    \item PostScript (Page Description Language)
  \end{itemize}
\end{itemize}

Simulation of a stack-based postorder traversal of the binary tree in Figure \ref{fig:BinaryTree}:\\
\begin{center}
{\footnotesize
\input{figures/stackTraversalPostorder}
}
\end{center}

\subsection{Breadth-First Search Traversal}

\begin{itemize}
  \item Breadth-First Search (BFS) traversal is a general graph traversal strategy that explores local or close nodes first before traversing ``deeper'' into the graph
  \item When applied to an oriented binary tree, BFS explores the tree level-by-level (top-to-bottom, left-to-right)
\end{itemize}

\subsection{Implementations \& Data Structures}

\begin{itemize}
  \item Reference based implementation: \mintinline{java}{TreeNode<T>}
  \begin{itemize}
    \item Owns (through composition) references to: \mintinline{java}{leftChild}, \mintinline{java}{rightChild}, \mintinline{java}{parent}
    \item Can use either \emph{sentinel} nodes or \mintinline{java}{null} to indicate missing children and parent
  \end{itemize}
  \item \mintinline{java}{BinaryTree<T>} owns a \mintinline{java}{root}
  \item SVN examples: \mintinline{java}{unl.cse.bst}
\end{itemize}

\subsubsection{Preorder Implementations}

  \begin{algorithm}[H]
    \Input{A binary tree node $u$}
    \Output{A preorder traversal of the nodes in the subtree rooted at $u$}
    print $u$ \;
    \texttt{preOrderTraversal}($u\rightarrow leftChild$) \;
    \texttt{preOrderTraversal}($u\rightarrow rightChild$) \;
    \caption{Recursive Preorder Tree Traversal}
    \label{algo:preOrderStack}
  \end{algorithm}
  
  Stack-based implementation:
  \begin{itemize}
    \item Initially, we push the tree's root into the stack
    \item Within a loop, we pop the top of the stack and process it
    \item We need to push the node's children for future processing
    \item Since a stack is LIFO, we push the right child first.
  \end{itemize}

  \begin{algorithm}[H]
    \Input{A binary tree, $T$}
    \Output{A preorder traversal of the nodes in $T$}
    $S \leftarrow$ empty stack \;
    \texttt{push} $T$'s root onto $S$ \;
    \While{$S$ is not empty}{
      $node \leftarrow$ \texttt{S.pop} \;
      \texttt{push} $node$'s right-child onto $S$ \;
      \texttt{push} $node$'s left-child onto $S$ \;
      process $node$ \;
     }
    \caption{Stack-based Preorder Tree Traversal}
    \label{algo:preOrderStack}
  \end{algorithm}

\subsubsection{Inorder Implementation}

Stack-based implementation:
\begin{itemize}
  \item The same basic idea: push nodes onto the stack as you visit them
  \item However, we want to delay processing the node until we've explored the left-sub-tree
  \item We need a way to tell if we are visiting the node for the first time or returning from the left-tree exploration
  \item To achieve this, we allow the node to be null
  \item If null, then we are returning from a left-tree exploration, pop the top of the stack and process (then push the right child)
  \item If not null, then we push it to the stack for later processing, explore the left child
\end{itemize}

  \begin{algorithm}[H]
    \Input{A binary tree, $T$}
    \Output{An inorder traversal of the nodes in $T$}
    $S \leftarrow$ empty stack \;
    $u \leftarrow root$ \;
    \While{$S$ is not empty \textsc{Or} $u \neq null$}{
      \uIf{$u \neq null$}{
        \texttt{push} $u$ onto $S$ \;
        $u \leftarrow u.\texttt{leftChild}$ \;
      }
     \Else{
        $u \leftarrow S.\texttt{pop}$ \;
        process $u$ \;
        $u \leftarrow u.\texttt{rightChild}$ \;
      }
     }
    \caption{Stack-based Inorder Tree Traversal}
    \label{algo:inOrderStack}
  \end{algorithm}

\subsubsection{Postorder Implementation}

Stack-based implementation:
\begin{itemize}
  \item Same basic ideas, except that we need to distinguish if we're visiting the node for the first time, second time or last (so that we can process it)
  \item To achieve this, we keep track of \emph{where} we came from: a parent, left, or right node
  \item We keep track of a previous and a current node
\end{itemize}

  \begin{algorithm}[H]
    \Input{A binary tree, $T$}
    \Output{A postorder traversal of the nodes in $T$}
    $S \leftarrow$ empty stack \;
    $prev \leftarrow null$ \;
    \texttt{push} root onto $S$ \;
    \While{$S$ is not empty}{
      $curr \leftarrow S.\texttt{peek}$ \;
      \uIf{$prev = null$ \textsc{Or} $prev.\texttt{leftChild} = curr$ \textsc{Or} $prev.\texttt{rightChild} = curr$}{
        \uIf{$curr.\texttt{leftChild} \neq null$}{
          \texttt{push} $curr.\texttt{leftChild}$ onto $S$ \;
        }
        \ElseIf{$curr.\texttt{rightChild} \neq null$}{
          \texttt{push} $curr.\texttt{rightChild}$ onto $S$ \;
        }
      }
      \uElseIf{$curr.\texttt{leftChild} = prev$}{
        \If{$curr.\texttt{rightChild} \neq null$}{
          \texttt{push} $curr.\texttt{rightChild}$ onto $S$ \;
        }
      }
      \Else{
        process $curr$ \;
        $S.\texttt{pop}$ \;
      }
      $prev \leftarrow curr$ \;
    }
    \caption{Stack-based Postorder Tree Traversal}
    \label{algo:postOrderStack}
  \end{algorithm}

\subsubsection{BFS Implementation}

  \begin{algorithm}[H]
    \Input{A binary tree, $T$}
    \Output{A BFS traversal of the nodes in $T$}
    $Q \leftarrow$ empty queue \;
    \texttt{enqueue} $T$'s root into $Q$ \;
    \While{$Q$ is not empty}{
      $node \leftarrow$ \texttt{Q.dequeue} \;
      \texttt{enqueue} $node$'s left-child onto $Q$ \;
      \texttt{enqueue} $node$'s right-child onto $Q$ \;
      print $node$ \;
     }
    \caption{Queue-based BFS Tree Traversal}
    \label{algo:queueBasedBFS}
  \end{algorithm}

\subsubsection{Tree Walk Implementations}
\begin{itemize}
  \item Simple rules based on local information: where you are and where you came from
  \item No additional data structures required
  \item Traversal is a ``walk'' around the perimeter of the tree
  \item Can use similar rules to determine when the current node should be processed to achieve pre, in, and postorder traversals
  \item Need to take care with corner cases (when current node is the root or children are missing)
  \item Pseudocode presented Algorithm \ref{algo:treeWalk}
\end{itemize}


  \begin{algorithm}
    \Input{A binary tree, $T$}
    \Output{A Tree Walk around $T$}
    $curr \leftarrow root$ \;
    $prevType \leftarrow parent$ \;
    \While{$curr \neq $\texttt{null}}{
      \uIf{$prevType = parent$}{
        \Comment{preorder: process $curr$ here}
        \uIf{$curr.leftChild$ exists}{
          \Comment{Go to the left child:}
          $curr \leftarrow curr.leftChild$ \;
          $prevType \leftarrow parent$ \;
        }
        \Else{
          $curr \leftarrow curr$ \;
          $prevType \leftarrow left$ \;
        }
      }
      \uElseIf{$prevType = left$}{
        \Comment{inorder: process $curr$ here}
        \uIf{$curr.rightChild$ exists}{
          \Comment{Go to the right child:}
          $curr \leftarrow curr.rightChild$ \;
          $prevType \leftarrow parent$ \;
        }
        \Else{
          $curr \leftarrow curr$ \;
          $prevType \leftarrow right$ \;
        }
      }
      \ElseIf{$prevType = right$}{
        \Comment{postorder: process $curr$ here}
        \uIf{$curr.parent = \texttt{null}$}{
          \Comment{root has no parent, we're done traversing}
          $curr \leftarrow curr.parent$ \;
        }
        \Comment{are we at the parent's left or right child?}
        \uElseIf{$curr = curr.parent.leftChild$}{
          $curr \leftarrow curr.parent$ \;
          $prevType \leftarrow left$ \;
        }
        \Else{
          $curr \leftarrow curr.parent$ \;
          $prevType \leftarrow right$ \;
        }
      }
    }
    \caption{Tree Walk based Tree Traversal}
    \label{algo:treeWalk}
  \end{algorithm}

%  \begin{algorithm}
%    \Input{A binary tree, $T$}
%    \Output{A Tree Walk around $T$}
%    $curr \leftarrow root$ \;
%    $prev \leftarrow \texttt{null}$ \;
%    \While{$curr \neq \texttt{null}$}{
%      \uIf{We came from $curr.parent$}{
%        %\Comment{process $curr$ here for preorder}
%        \uIf{$curr.leftChild$ exists}{
%          \Comment{To to left child:}
%          $prev \leftarrow curr$ \;
%          $curr \leftarrow curr.leftChild$ \;
%        }
%        \uElseIf{$curr.rightChild$ exists}{
%          \Comment{To to right child:}
%          $prev \leftarrow curr$ \;
%          $curr \leftarrow curr.rightChild$ \;
%        }
%        \Else{
%          \Comment{To to parent:}
%          $prev \leftarrow curr$ \;
%          $curr \leftarrow curr.parent$ \;
%        }
%      }
%      \uElseIf{We came from $curr.leftChild$}{
%        %\Comment{process $curr$ here for inorder}
%        \uIf{$curr.rightChild$ exists}{
%          \Comment{To to right child:}
%          $prev \leftarrow curr$ \;
%          $curr \leftarrow curr.rightChild$ \;
%        }
%        \Else{
%          \Comment{Go to parent:}
%          $prev \leftarrow curr$ \;
%          $curr \leftarrow curr.parent$ \;
%        }
%      }
%      \ElseIf{We came from $curr.rightChild$}{
%        %\Comment{process $curr$ here for inorder}
%        \Comment{Go to parent: }
%        $prev \leftarrow curr$ \;
%        $curr \leftarrow curr.parent$ \;
%      }
%    }
%    \caption{Tree Walk based Tree Traversal}
%    \label{algo:treeWalk}
%  \end{algorithm}

%Suppose we are currently at node $u$ and we:
%\begin{itemize}
%  \item Came from $u$'s parent, then:
%  \begin{itemize}
%    \item Go to $u$'s left-child if it exists, otherwise
%    \item Go to $u$'s right-child if it exists
%    \item Otherwise, return to $u$'s parent
%  \end{itemize}
%  \item Came from $u$'s left-child, then:
%  \begin{itemize}
%    \item Go to $u$'s right-child if it exists
%    \item Otherwise, return to $u$'s parent
%  \end{itemize}
%  \item Came from $u$'s right-child, then:
%  \begin{itemize}
%    \item Return to $u$'s parent
%  \end{itemize}
%\end{itemize}
%
%\begin{table}
%\centering
%\begin{tabular}{|l|l|l|}
%\hline
%We are at: & and came from: & then traverse next to: \\
%\hline
%\end{tabular}
%\caption{A}
%\end{table}


\subsection{Operations}

Basic Operations:
\begin{itemize}
  \item Search for a particular element/key
  \item Adding an element
  \begin{itemize}
    \item Add at most shallow available spot
    \item Add at a random leaf
    \item Add internally, shift nodes down by some criteria
  \end{itemize}
  \item Removing elements
  \begin{itemize}
    \item Removing leaves
    \item Removing elements with one child
    \item Removing elements with two children
  \end{itemize}
\end{itemize}

Other Operations:
\begin{itemize}
  \item Compute the total number of nodes in a tree
  \item Compute the total number of leaves in a tree
  \item Given an item or node, compute its depth
  \item Compute the depth of a tree
\end{itemize}

\section{Binary Search Trees}

Regular binary search trees have little structure to their elements; search, insert, delete operations are
still linear with respect to the number of tree nodes, $O(n)$.  We want a data structure with operations
proportional to its depth, $O(d)$.  To this end, we add structure and order to tree nodes.

\begin{itemize}
  \item Each node has an associated \emph{key}
  \item Binary Search Tree Property: For every node $u$ with key $u_k$ in $T$
  \begin{enumerate}
    \item Every node in the left-sub-tree of $u$ has keys \emph{less} than $u_k$
    \item Every node in the right-sub-tree of $u$ has keys \emph{greater} than $u_k$
  \end{enumerate}
  \item Duplicate keys can be handled, but you must be consistent and not guaranteed to be contiguous
  \item Alternatively: do not allow duplicate keys or define a key scheme that ensures a \emph{total order}
  \item Inductive property: all sub-trees are also binary search trees
  \item A full example can be found in Figure \ref{fig:binarySearchTreeExample}
\end{itemize}

\begin{figure}[h]
\centering
\input{figures/bstFig}
\caption{A Binary Search Tree}
\label{fig:binarySearchTreeExample}
\end{figure}

\subsection{Basic Operations}

Observation: a binary search tree has more \emph{structure}: the key in each node provides information on 
where a node is \emph{not} located.  We will exploit this structure to achieve $O(\log{n})$ operations.

Search/retrieve
\begin{itemize}
  \item Goal: find a node (and its data) that matches a given key $k$
  \item Start at the node
  \item At each node $u$, compare $k$ to $u$'s key, $u_k$:
  \begin{itemize}
    \item If equal, element found, stop and return
    \item If $k < u_k$, traverse to $u$'s left-child
    \item If $k > u_k$, traverse to $u$'s right-child
  \end{itemize}
  \item Traverse until the sub-tree is empty (element not found)
  \item Analysis: number of comparisons is bounded by the depth of the tree, $O(d)$
\end{itemize}

  \begin{algorithm}[H]
    \Input{A binary search tree, $T$, a key $k$}
    \Output{The tree node $u \in T$ whose key, $u_k$ matches $k$}
    $u \leftarrow$ $T$'s root \;
    \While{$u \neq \phi$}{
      \If{$u_k = k$}{
        output $u$ \;
      }
      \uElseIf{$u_k > k$}{
        $u \leftarrow$ $u$'s left-child \;
      } \uElseIf{$u_k < k$}{
        $u \leftarrow$ $u$'s left-child \;
      }
    }
    output $\phi$ \;
    \caption{Search algorithm for a binary search tree}
    \label{algo:bstSearch}
  \end{algorithm}

Insert
\begin{itemize}
  \item Insert new nodes as leaves
  \item To determine where it should be inserted: traverse the tree as above
  \item Insert at the first available spot (first missing child node)
  \item Analysis: finding the available location is $O(d)$, inserting is just reference juggling, $O(1)$
\end{itemize}

Delete
\begin{itemize}
  \item Need to first \emph{find} the node $u$ to delete, traverse as above, $O(d)$
  \item If $u$ is a leaf (no children): its safe to simply delete it
  \item If $u$ has one child, then we can ``promote'' it to $u$'s spot ($u$'s parent will now point to $u$'s child)
  \item If $u$ has two children, we need to find a way to preserve the BST property
  \begin{itemize}
    \item Want to minimally change the tree's structure
    \item Need the operation to be efficient
    \item Find the minimum element of the greater nodes (right sub-tree) or the maximal element of the lesser nodes (left sub-tree)
    \item Such an element will have at most one child (which we know how to delete)
    \item Delete it and store off the key/data
    \item Replace $u$'s key/data with the contents of the minimum/maximum element
  \end{itemize}
  \item Analysis:
  \begin{itemize}
    \item Search/Find: $O(d)$
    \item Finding the min/max: $O(d)$
    \item Swapping: $O(1)$
    \item In total: $O(d)$
  \end{itemize}
  \item Examples illustrated in Figure \ref{fig:bstOperations}
\end{itemize}

\input{figures/bstOperationsFig}

\section{Balanced Binary Search Trees}
\index{Balanced Binary Search Trees}\index{Binary Search Trees!Balanced}

\input{sectionBalancedBinarySearchTrees}

\section{Heaps}

\begin{definition}
A \emph{heap} is a binary tree that satisfies the following properties.
\begin{enumerate}
  \item It is a \emph{full} or \emph{complete} binary tree: all nodes are present except possibly the last row
  \item If the last row is not full, all nodes are full-to-the-left
  \item It satisfies the \emph{Heap Property}: every node has a key that is greater than both of its children (max-heap)
\end{enumerate}
\end{definition}

\begin{itemize}
  \item As a consequence of the Heap Property, the maximal element is always at the root
  \item Alternatively, we can define a \emph{min-heap}
  \item Variations: 2-3 heaps, fibonacci heaps, etc.
  \item A min-heap example can be found in Figure \ref{fig:minHeap}
\end{itemize}

\begin{figure}[h]
\centering
\input{figures/heapFig}
\caption{A min-heap}
\label{fig:minHeap}
\end{figure}

Applications
\begin{itemize}
  \item Heaps are an optimal implementation of a priority queue
  \item Used extensively in other algorithms (Heap Sort, Prim's, Dijkstra's, Huffman Coding, etc.) to ensure efficient operation
\end{itemize}

\subsection{Operations}

Insert
\begin{itemize}
  \item Want to preserve the full-ness property and the Heap Property
  \item Preserve full-ness: add new element at the end of the heap (last row, first free spot on the left)
  \item Insertion at the end may violate the Heap Property
  \item Heapify/fix: bubble up inserted element until Heap Property is satisfied
  \item Analysis: insert is $O(1)$; heapify: $O(d)$
\end{itemize}

Remove from top
\begin{itemize}
  \item Want to preserve the full-ness property and the Heap Property
  \item Preserve full-ness: swap root element with the last element in the heap (lowest row, right-most element)
  \item Heap property may be violated
  \item Heapify/fix: bubble new root element down until Heap Property is satisfied
  \item Analysis: Swap is $O(1)$; heapify: $O(d)$
\end{itemize}

Others
\begin{itemize}
  \item Arbitrary remove
  \item Find
  \item Possible, but not ideal: Heaps are restricted-access data structures
\end{itemize}

Analysis
\begin{itemize}
  \item All operations are $O(d)$
  \item Since Heaps are \emph{full}, $d = O(\log{n})$
  \item Thus, all operations are $O(\log{n})$
\end{itemize}

\subsection{Implementations}

Array-Based
\begin{itemize}
  \item Root is located at index $1$
  \item If a node $u$ is at index $i$, $u$'s left-child is at $2i$, its right-child is at $2i+1$
  \item If node $u$ is at index $j$, its parent is at index $\lfloor \frac{j}{2} \rfloor$
  \item Alternatively: 0-index array left/right children/parent are at $2n+1, 2n+2$, and $\lfloor \frac{j-1}{2} \rfloor$
  \item Advantage: easy implementation, all items are contiguous in the array (in fact, a BFS ordering!)
  \item Disadvantage: Insert operation may force a reallocation, but this can be done in amortized-constant time (though may still have wasted space)
\end{itemize}

Tree-Based
\begin{itemize}
  \item Reference-based tree (nodes which contain references to children/parent)
  \item Parent reference is now \emph{required} for efficiency
  \item For efficiency, we need to keep track of the \emph{last} element in the tree
  \item For deletes/inserts: we need a way to find the last element and first ``open spot'' 
  \item We'll focus on finding the first available open spot as the same technique can be used to find the last element with minor modifications
\end{itemize}

\subsubsection{Finding the first available open spot in a Tree-based Heap}

Technique A: numerical technique
\begin{itemize}
  \item WLOG: assume we keep track of the number of nodes in the heap, $n$ and thus the depth $d = \lfloor \log{n} \rfloor$
  \item If $n = 2^{d+1} - 1$ then the tree is full, the last element is all the way to the right, the first available spot is all the way to the left
  \item Otherwise $n < 2^{d+1} - 1$ and the heap is not full (the first available spot is located at level $d$, root is at level $0$)
  \item Starting at the root, we want to know if the last element is in the left-subtree or the right subtree
  \item Let $m = n - (2^d - 1)$, the number of nodes present in level $d$
  \item If $m \geq \frac{2^d}{2}$ then the left-sub tree is full at the last level and so the next open spot would be in the right-sub tree
  \item Otherwise if $m < \frac{2^d}{2}$ then the left-sub tree is not full at the last level and so the next open spot is in the left-sub tree
  \item Traverse down to the left or right respectively and repeat: the resulting sub-tree will have depth $d - 1$ with $m = m$ (if traversing left)
  	or $m = m - \frac{2^d}{2}$ (if traversing right)
  \item Repeat until we've found the first available spot
  \item Analysis: in any case, its $O(d) = O(\log{n})$ to traverse from the root to the first open spot
\end{itemize}

\begin{figure}
\centering
\input{figures/figHeapAnalysis}
\caption[Tree-based Heap Analysis]{Tree-based Heap Analysis.  Because of the fullness property, 
we can determine which subtree (left or right) the ``open'' spot in a heap's tree is by
keeping track of the number of nodes, $n$.  This can be inductively extended to each subtree until
the open spot is found.}
\label{fig:heapAnalysis}
\end{figure}


  \begin{algorithm}[H]
    \Input{A tree-based heap $H$ with $n$ nodes}
    \Output{The node whose child is the next available open spot in the heap}
    curr $\leftarrow T.head$ \;
    $d \leftarrow \lfloor \log{n} \rfloor$ \;
    $m \leftarrow n$ \;
    \While{curr has both children}{
      \eIf{$m = 2^{d+1} - 1$}{
        \Comment{remaining tree is full, traverse all the way left}
        \While{curr has both children}{
          $curr \leftarrow curr.leftChild$\;
        }
      }{
        \Comment{remaining tree is not full, determine if the next open spot is in the left or right sub-tree}
        \eIf{$m \geq \frac{2^d}{2}$}{
          \Comment{left sub-tree is full}
          $d \leftarrow (d-1)$ \;
          $m \leftarrow (m - \frac{2^d}{2})$ \;
          $curr \leftarrow curr.rightChild$ \;
        }{
          \Comment{left sub-tree is not full}
          $d \leftarrow (d-1)$ \;
          $m \leftarrow m$ \;          
          $curr \leftarrow curr.leftChild$ \;
        }
      }
    }
    output $curr$ \;
    \caption{Find Next Open Spot - Numerical Technique}
    \label{algo:heapFind}
  \end{algorithm}

Technique B: Walk Technique
\begin{itemize}
  \item Alternatively, we can adapt the idea behind the tree walk algorithm to find the next available open spot
  \item We'll assume that we've kept track of the last node
  \item If the tree is full, we simply traverse all the way to the left and insert, $O(d)$
  \item If the last node is a left-child then its parent's right child is the next available spot, finding it is $O(1)$
  \item Otherwise, we'll need to traverse around the perimeter of the tree until we reach the next open slot
\end{itemize}

  \begin{algorithm}[H]
    \Input{A tree-based heap $H$ with $n$ nodes}
    \Output{The node whose (missing) child is the next available open spot in the heap}
      $d \leftarrow \lfloor \log{n} \rfloor$ \;
      \uIf{$n = 2^{d+1} - 1$}{
        \Comment{The tree is full, traverse all the way to the left}
        $curr \leftarrow root$ \;
        \While{$curr.leftChild \neq null$}{
          $curr \leftarrow curr.leftChild$ \;
        }
      }
      \uElseIf{$last$ is a left-child}{
        \Comment{parent's right child is open}
        $curr \leftarrow last.parent$ \;
      }
      \Else{
        \Comment{The open spot lies in a subtree to the right of the last node}
        \Comment{Walk the tree until we reach it}
        $curr \leftarrow last.parent$ \;
        \While{$curr$ is a right-child}{
          $curr \leftarrow curr.parent$ \;
        }
        \Comment{"turn" right}
        $curr \leftarrow curr.parent$ \;
        $curr \leftarrow curr.rightChild$ \;
        \Comment{traverse all the way left}
        \While{$curr.leftChild \neq null$}{
          $curr \leftarrow curr.leftChild$ \;
        }        
      }
      \Comment{current node's missing child is the open spot}
      output $curr$ \;
    \caption{Find Next Open Spot - Walk Technique}
    \label{algo:heapFindWalk}
  \end{algorithm}


\subsection{Java Collections Framework}

Java has support for several data structures supported by underlying tree structures.

\begin{itemize}
  \item \mintinline{java}{java.util.PriorityQueue<E>} is a binary-heap based priority queue
  \begin{itemize}
    \item Priority (keys) based on either \emph{natural ordering} or a provided \mintinline{java}{Comparator}
    \item Guaranteed $O(\log{n})$ time for insert (\mintinline{java}{offer}) and get top (\mintinline{java}{poll})
    \item Supports $O(n)$ arbitrary \mintinline{java}{remove(Object)} and search (\mintinline{java}{contains}) methods
  \end{itemize}
  \item \mintinline{java}{java.util.TreeSet<E>}
  \begin{itemize}
    \item Implements the \mintinline{java}{SortedSet} interface; makes use of a \mintinline{java}{Comparator}
    \item Backed by \mintinline{java}{TreeMap}, a red-black tree balanced binary tree implementation
    \item Guaranteed $O(\log{n})$ time for add, remove, contains operations
    \item Default iterator is an in-order traversal
  \end{itemize}
\end{itemize}

\subsection{Other Operations}

include decrease key operation here

\subsection{Variations}

Binomial, Fibonacci, etc.

\section{Applications}

\subsection{Heap Sort}

\begin{itemize}
  \item If min/max element is always at the top; simply insert all elements, then remove them all!
  \item Perfect illustration of ``Smart data structures and dumb code are a lot better than the other way around''
\end{itemize}

  \begin{algorithm}[H]
    \Input{A collection of elements $A = \{a_1, \ldots, a_n\}$}
    \Output{A collection, $A'$ of elements in $A$, sorted}
    $H \leftarrow $ empty heap \;
    $A' \leftarrow $ empty collection \;
    \ForEach{$x \in A$}{
      insert $x$ into $H$
    }
    \While{$H$ is not empty}{
      $y \leftarrow $ remove top from $H$ \;
      Add $y$ to the end of $A'$ \;
    }
    output $A'$ \;
    \caption{Heap Sort}
    \label{algo:heapSort}
  \end{algorithm}

Analysis
\begin{itemize}
  \item Amortized analysis: insert/remove operations are not constant throughout the algorithm
  \item On first iteration: insert is $d = O(1)$; on the $i$-th iteration, $d = O(\log{i})$; only on the last iteration is insertion $O(\log{n})$
  \item In total, the insert phase is:
	$$\sum_{i=1}^{n} \log{i} = O(n\log{n})$$
  \item A similar lower bound can be shown
  \item Same analysis applies to the remove phase:
	$$\sum_{i=n}^{1} \log{i}$$
  \item In total, $O(n\log{n})$
\end{itemize}


\subsection{Huffman Coding}

Overview
\begin{itemize}
  \item Coding Theory is the study and theory of \emph{codes}---schemes
    for transmitting data
  \item Coding theory involves efficiently padding out data with 
	redundant information to increase reliability (detect or even correct
	errors) over a noisy channel
  \item Coding theory also involves \emph{compressing} data to save space
    \begin{itemize}
      \item MP3s (uses a form of Huffman coding, but is
            information lossy)
      \item jpegs, mpegs, even DVDs
      \item \texttt{pack} (straight Huffman coding)
      \item \texttt{zip}, \texttt{gzip} (uses a Ziv-Lempel and Huffman
            compression algorithm)
    \end{itemize}
\end{itemize}

Basics
\begin{itemize}
  \item Let $\Sigma$ be a fixed \emph{alphabet} of size $n$
  \item A \emph{coding} is a mapping of this alphabet to a collection of binary \emph{codewords},
	$$\Sigma \rightarrow \{0, 1\}^*$$
  \item A \emph{block encoding} is a \emph{fixed length encoding} scheme where all
	codewords have the same length (example: ASCII); requires $\lceil \log_2{n} \rceil$ length codes
  \item Not all symbols have the same frequency, alternative: \emph{variable length encoding}
  \item Intuitively: assign shorter codewords to more frequent symbols, longer to less frequent symbols
  \item Reduction in the overall \emph{average} codeword length
  \item Variable length encodings must be \emph{unambiguous}
  \item Solution: \emph{prefix free codes}: a code in which no \emph{whole} codeword is the
	prefix of another (other than itself of course).
  \item Examples:
     \begin{itemize}
      \item $\{0, 01, 101, 010\}$ is not a prefix free code.
      \item $\{10, 010, 110, 0110\}$ is a prefix free code.
     \end{itemize}
  \item A simple way of building a prefix free code is to associate codewords
    with the \emph{leaves} of a binary tree (not necessarily full).
  \item Each edge corresponds to a bit, 0 if it is to the left sub-child and
    1 to the right sub-child.
  \item Since no simple path from the root to any leaf can continue to another
    leaf, then we are guaranteed a prefix free coding.
  \item Using this idea along with a greedy encoding forms the basis of
    \emph{Huffman Coding}
\end{itemize}

Steps
\begin{itemize}
  \item Consider a precomputed relative frequency function:
	$$\mathrm{freq} : \Sigma \rightarrow [0, 1]$$
  \item Build a collection of \emph{weighted} trees $T_x$ for each symbol $x\in Sigma$ with $wt(T_x) = \mathrm{freq}(x)$
  \item Combine the two least weighted trees via a new node; associate a new weight (the sum of the weights of the two 
	subtrees)
  \item Keep combining until only one tree remains
  \item The tree constructed in Huffman's algorithm is known as a
    \emph{Huffman Tree} and it defines a \emph{Huffman Code}
\end{itemize}

  \begin{algorithm}[H]
    \Input{An alphabet of symbols, $\Sigma$ with relative frequencies, $\mathrm{freq}(x)$}
    \Output{A Huffman Tree}
    $H \leftarrow $ new min-heap \;
    \ForEach{$x \in \Sigma$}{
      $T_x \leftarrow$ single node tree \;
      $wt(T_x) \leftarrow \mathrm{freq}(x)$ \;
      insert $T_x$ into $H$ \;
    }
    \While{size of $H > 1$}{
      $T_r \leftarrow$ new tree root node \;
      $T_a \leftarrow H.getMin$ \;
      $T_b \leftarrow H.getMin$ \;
      $T_r.leftChild \leftarrow T_a$ \;
      $T_r.rightChild \leftarrow T_b$ \;
      $wt(r) \leftarrow wt(T_a) + wt(T_b)$ \;
      insert $T_r$ into $H$ \;
    }
    output $H.getMin$ \;
    \caption{Huffman Coding}
    \label{algo:huffmanCoding}
  \end{algorithm}

\subsubsection{Example}
     Construct the Huffman Tree and Huffman Code for a file with the
     following content.
    \begin{center}
    \begin{tabular}{l|cccccccc}
      character & A & B & C & D & E & F & G \\
      \hline
      frequency & 0.10 & 0.15 & 0.34 & .05 & .12 & .21 & .03\\
    \end{tabular}
    \end{center}

\begin{figure}
\centering
\input{figures/huffmanTree}
\label{fig:huffmanTree}
\caption{Huffman Tree}
\end{figure}

\begin{itemize}
  \item Average codeword length:
    $$.10 \cdot 3 + .15 \cdot 3 + .34 \cdot 2 + .05 \cdot 4 + .12 \cdot 3 + .21 \cdot 2
    + .03 \cdot 4 = 2.53$$
  \item Compression ratio: 
    $$\frac{(3-2.53)}{3} = 15.67\%$$
  \item In general, for text files, \texttt{pack} (Huffman Coding), claims an average
    compression ratio of 25-40\%.
  \item Degenerative cases:
    \begin{itemize}
      \item When the probability distribution is uniform: $p(x) = p(y)$
      for all $x, y \in \Sigma$
      \item When the probability distribution follows a fibonacci
      sequence (the sum of each of the two smallest probabilities is
      less than equal to the next highest probability for all probabilities)
    \end{itemize}
\end{itemize}

Other schemes

\begin{itemize}
  \item \url{https://ro-che.info/articles/2017-08-20-understanding-ans}
\end{itemize}

