%!TEX root = ComputerScienceThree.tex

\section{Introduction}

TODO

\section{Solving Linear Systems}

A system of linear equations is a collection of $n$ equalities in $n$ unknowns.  An example:

    $$\begin{array}{lcl}
    3x + 2y & = & 35\\
    x + 3y & = & 27\\
    \end{array}$$

We are interested in solving such systems algorithmically.  A generalized form:

    $$\begin{array}{lcl}
    a_{11}x_1 + a_{12}x_2 + \cdots + a_{1n}x_n & = & b_1\\
    a_{21}x_1 + a_{22}x_2 + \cdots + a_{2n}x_n & = & b_2\\
    \vdots & ~ & ~\\
    a_{n1}x_1 + a_{n2}x_2 + \cdots + a_{nn}x_n & = & b_n\\
    \end{array}$$

In general, we want to eliminate variables until we have 1 equation in 1 unknown.

The goal of \emph{Guassian Elimination}\index{Gaussian Elimination} is to transform a system of
linear equations in $n$ unknowns to an equivalent,
\emph{upper-triangular} system.

    An upper triangular system is one in which everything below and to
    the left of the diagonal on the matrix is zero.  In general we would
    want something like

    $$\begin{array}{llllcl}
    a_{11}'x_1 + & a_{12}'x_2 + & \ldots + & a_{1n}'x_n & = & b_1'\\
    ~ &            a_{22}'x_2 + & \ldots + & a_{2n}'x_n & = & b_2'\\
    ~ & ~ & ~ & ~ & ~ & \vdots\\
    ~ & ~ & ~ & a_{nn}'x_n & = & b_n'\\
    \end{array}$$

    A system of linear equations can be thought of as an $n \times n$
    matrix with a variable vector $x = [x_1, x_2, \ldots x_n]$ and a
    solution vector $b = [b_1', b_2', \ldots b_n']$.  In
    other words, we can equivalently solve the matrix equation
    $$Ax = b \rightarrow A'x = b'$$

    An upper-triangular system is easier to solve because we can easily
    back-track (from the bottom right) and solve each unknown.  This
    is known as \emph{backward substitution} (not to be confused with
    the method for solving recurrence relations).

    Now that we know how to back-solve, how do we take a given system
    and transform it to an upper-triangular system?  We use several
    elementary row operations:
    \begin{itemize}
      \item We can exchange any two \emph{rows} of a system
      \item We can exchange any two \emph{columns} of a system
      \footnote{If we do, however, we must make note that the \emph{variables}
      have also been exchanged.  For instance if we exchange column $i$
      and column $j$ then $x_i$ and $x_j$ have exchanged.}
      \item We can multiply an equation by a non-zero scalar
      \item We can add or subtract one equation with another (or a multiple thereof)
    \end{itemize}

    Such operations \emph{do not} change the solution to the system.

\subsubsection{Example}

Consider the following system of linear equations:
$$\begin{array}{lcl}
3x_1 - x_2 + 2x_3 & = 1 \\
4x_1 + 2x_2 - x_3 & = 4 \\
x_1 + 3x_2 + 3x_3 & = 5
\end{array}$$

$$\begin{bmatrix}
 3 & -1 & 2 & 1 \\
 4 & 2 & -1 & 4 \\
 1 & 3 & 3 & 5
\end{bmatrix}$$

$$
\begin{bmatrix}
 3 & -1 & 2 & 1 \\
 4 & 2 & -1 & 4 \\
 1 & 3 & 3 & 5
\end{bmatrix}
\begin{array}{c}
  \Rightarrow \\
  \textrm{subtract $\frac{4}{3}$ times row 1} \\
  \textrm{subtract $\frac{1}{3}$ times row 1} \\
\end{array}
$$
$$
\begin{bmatrix}
 3 & -1 & 2 & 1 \\
 0 & \frac{10}{3} & -\frac{11}{3} & \frac{8}{3} \\
 0 & \frac{10}{3} & \frac{7}{3} & \frac{14}{3}
\end{bmatrix}
\begin{array}{c}
  \Rightarrow \\
  ~ \\
  \textrm{subtract $1$ times row 2} \\
\end{array}
$$

$$
\begin{bmatrix}
 3 & -1 & 2 & 1 \\
 0 & \frac{10}{3} & -\frac{11}{3} & \frac{8}{3} \\
 0 & 0 & 6 & 2
\end{bmatrix}
$$

Back solving gives us:
$$\vec{x} = (x_1, x_2, x_3) = \left(\frac{1}{2}, \frac{7}{6}, \frac{1}{3}\right)$$


    Using elementary row operations, we can proceed as follows.  Using $a_{11}$
    as a pivot, we subtract $a_{21} / a_{11}$ times the first equation from
    the second equation, and continue with $a_{31} / a_{11}$ from the third, etc.
    In general, we subtract $a_{i1} / a_{11}$ times row $1$ from the $i$-th row
    for $2 \leq i \leq n$.  We repeat for $a_{22}$ as the next pivot, etc.

Issues:

\begin{itemize}
  \item It may be the case that we attempt to divide by zero, which is
      invalid.  To solve this problem, we can exchange a row!
  \item Though a divisor may not be zero, it could be so small that
      our quotient is too large.  We can solve this one of two ways--
      either scale the current row so that it becomes 1 or we can again exchange
      rows.  Specifically, the row with the largest absolute value in the
      $i$-th row.
  \item The system may be \emph{indeterminant}: there may be one or more free-variables (an infinite number of solutions).  Visually, both
  equations correspond to the same line (intersecting at an infinite number of points); example:
	$$\begin{array}{rcl}
	  x + 7y & = & 8 \\
	  2x + 14y & = & 16
	\end{array}$$
  \item The system may be \emph{inconsistent} (there are \emph{no} solutions).  Visually the equations correspond to two parallel
  lines that never intersect.  Example:
	$$\begin{array}{rcl}
	  x + 7y & = & 8 \\
	  2x + 14y & = & 20
	\end{array}$$
\end{itemize}

Back Solving: once we have an upper triangular matrix, we can solve for each variable. 
We do so in reverse, solving for $x_n$ first, then for each prior variable.  In 
general, 
  $$x_i = \frac{b_i - a_{i,i+1}x_{i+1} - a_{i,i+2}x_{i+2} - \cdots a_{i,n}x_{n}}{a_{i,i}}$$
  
\input{figures/figureGaussianEliminationIllustration.tex}  

  \begin{algorithm}[H]
    \Input{An $n\times n$ matrix $A$ and a $1 \times n$ vector $b$}
    \Output{An augmented, upper triangluar matrix $A'$ preserving the solution to $A \cdot \vec{x} = \vec{b}$}
    $A' \leftarrow A | \vec{b}$ \Comment{augment}
    \For{$i = 1, \ldots n-1$} {
      $pivotrow \leftarrow i$ \;
      \For{$j = (i+1), \ldots, n$}{
        \If{$|A'[j,i]| > |A'[pivotrow, i]|$}{
          $pivotrow \leftarrow j$ \;
        }
      }
      \For{$k = i, \ldots, (n+1)$}{
        swap($A'[i,k], A'[pivotrow,k]$) \;
      }
      \For{$j = (i+1), \ldots, n$}{
        $t \leftarrow \frac{A'[j,i]}{A'[i,i]}$ \;
        \For{$k = i, \ldots, (n+1)$}{
          $A'[j,k] \leftarrow A'[j,k] - A'[i,k] \cdot t$ \;
        }
      }
    }
    output $A'$ \;
    \caption{(Better) Gaussian Elimination}
    \label{algo:gaussianElimination}
  \end{algorithm}

  \begin{algorithm}[H]
    \Input{An augmented, upper triangular $(n \times n+1)$ matrix $A$}
    \Output{A solution vector $\vec{x} = (x_1, \ldots, x_n)$}
    \For{ $i=n, \ldots 1$ } {
      $t \leftarrow A[i, n+1]$ \Comment{set to $b_i'$}
      \For{$j = (i+1), \ldots, n$}{
        $t \leftarrow  t - x_j \cdot A[i, j]$ \;
      }
      $x_i \leftarrow \frac{t}{A[i, i]}$ \;
    }
    output $\vec{x}$ \;
    \caption{Back Solving}
    \label{algo:backSolving}
  \end{algorithm}

\subsubsection{Analysis}

The multiplication is our elementary operation.  We thus have 
the following summation:
    $$C(n) = \sum_{i=1}^{n-1}\sum_{j=i+1}^n\sum_{k=i}^{n+1}1 \approx \frac{1}{3}
    n^3 \in \Theta(n^3)$$
    Backward substitution will work on the remaining entries making about
    $\frac{n(n-1)}{2}$ multiplications and $n$ divisions, so the asymptotic
    classification doesn't change.

\subsection{LU Decomposition}
\index{LU Decomposition}

    Performing Gaussian Elimination on a matrix $A$ can give us to additional
    matrices:
    \begin{itemize}
      \item $L$ -- A lower-triangular matrix with 1's on the diagonal and lower
      entries corresponding to the row multiples used in Gaussian elimination.
      \item $U$ -- The upper-triangular matrix as a result of Gaussian elimination
      on $A$.
    \end{itemize}

LU-Decomposition of the previous example:

$$L =
\begin{bmatrix}
 1 & 0 & 0 \\
 \frac{4}{3} & 1 & 0 \\
 \frac{1}{3} & 1 & 1
\end{bmatrix}
U =
\begin{bmatrix}
 3 & -1 & 2 \\
 0 & \frac{10}{3} & -\frac{11}{3} \\
 0 & 0 & 6
\end{bmatrix}
$$

    It is not hard to prove that
    $$A = L\cdot U$$
    where $L \cdot U$ is the usual matrix multiplication.  We observe then,
    that solving the system $Ax = b$ is equivalent to solving
    $$LU\vec{x} = \vec{b}$$

    We do this by performing Gaussian Elimination, then we solve the system
    $Ly = b$ where $y = Ux$ then we solve $Ux = y$.

    Though this is essentially
    Gaussian elimination with a bit more work, it has the advantage that
    once we have the decomposition $L$ and $U$, we can solve for \emph{any}
    vector $b$---a huge advantage if we were to make changes to the $\vec{b}$ 
    vector.
    
\subsection{Matrix Inverse}
\index{Matrix Inverse}

    The \emph{inverse} of an $n \times n$ matrix $A$ is another
    matrix $A^{-1}$ such that
    $$AA^{-1} = I$$
    where $I$ is the identity matrix (1's on the diagonal, 0's everywhere
    else).  The inverse of a matrix is always unique.  \emph{However}
    not every matrix is invertible--such matrices are called \emph{singular}.

    Recall that when using Gaussian elimination, the system may be inconsistent.
    That is, no valid solution exists.  One can prove that this is the case if
    and only if $A$ is not invertible which in turn is the case if and only if
    one of the rows is a linear combination of another row.

    Such a linear combination can be detected by running Gaussian
    Elimination on $A$.  If one of the diagonal entries is 0 then
    $A$ is singular.  Otherwise $A$ is consistent.

    Finding an inverse is important because it allows us an implicit division
    operation among matrices\footnote{be careful: remember, matrix multiplication
    is \emph{not} commutative in general}:

    $$A^{-1} A x = A^{-1}b$$
    $$x = A^{-1}b$$

    We can use Gaussian Elimination to find a matrix's inverse by solving
    $$A X = I$$
    where $X$ are $n^2$ unknowns in the inverse matrix.  We do this by solving
    $n$ systems of linear equations with the same coefficient matrix $A$
    and a vector of unknowns $x^j$ (the $j$-th column of $X$
    and a solution vector $e^j$ (the $j$-th column of the identity matrix):
    $$Ax^j = e^j$$
    for $1 \leq j \leq n$

Alternatively, you can adapt Gaussian Elimination to compute an inverse
as follows.  Given $A$, you create a ``super augmented'' matrix, 
  $$\left[ A \big| I \right]$$
Where $I$ is the $n \times n$ identity matrix, resulting in an $n \times 2n$.
Then you perform Gaussian Elimination to make the left part upper
triangular, then perform a Gaussian Elimination for the upper part as
well, then normalize each row so that each entry on the diagonal is 1 
(this is actually Gauss-Jordan elimination).  The resulting augmented matrix
represents
  $$\left[ I \big| A^{-1} \right]$$


\subsection{Determinants}
\index{Determinant}

The \emph{determinant}\index{Determinant} of an $n \times n$ square 
matrix is a measure of its coefficients which has several interpretations.  
With respect to \emph{linear transformations}, it can quantify ``area'' or
``volume''.  

The determinant has several equivalent definitions.

$$\det{A} = \sum_{\sigma \in S_n} \left[ \mathrm{sgn}(\sigma) \prod_{i=1}^n A_{i, \sigma_i} \right]$$
where $S_n$ is the symmetric group on $n$ elements (the set of all permutations).

Alternatively, it can be recursively defined as:

    $$\det A = \sum_{j=1}^n s_j a_{1j}\det{A_j}$$
    where
    $$s_j = \left\{\begin{array}{ll}
     1 & \textrm{if $j$ is odd}\\
     -1 & \textrm{if $j$ is even}
     \end{array}\right.$$
   \begin{itemize}
     \item $a_{1j}$ is the element in row 1 and column $j$
     \item $A_j$ is the $(n-1) \times (n-1)$ matrix obtained by deleting row 1
     and column $j$ from $A$
     \item If $n = 1$ then $\det{A} = a_{11}$
   \end{itemize}

    Determinants are easy to compute for $2 \times 2$ matrices and even
    $3 \times 3$ matrices.  There is also an easy to remember \emph{visual}
    way to do it.

    If you can easily compute a determinant you can easily tell if a matrix
    is invertible or not:

    \begin{theorem}
    An $n \times n$ matrix is invertible if and only if $\det{A} \neq 0$
    \end{theorem}

    If you were to use the definition to compute a determinant, you would
    be summing $n!$ terms!  Instead we can again use Gaussian
    elimination.

    The determinant of an upper-triangular matrix is simply the product of
    the elements on its diagonal.  Thus we can use Gaussian elimination to
    transform $A$ into an upper-triangular matrix $A'$ and simply compute
    $$\det{A'} = \prod_{i=1}^n a_{ii}'$$

    Some things to keep in mind about determinants when using Gaussian
    elimination:

    \begin{itemize}
      \item Realize that $\det{(A + B)} \neq \det{A} + \det{B}$
      \item However, it is true that $\det{AB} = \det{A}\det{B}$
      \item Also, $\det{A} = \det{A^T}$
      \item If we multiply a row by a constant $c$, then $\det{A} = \det{A'} / c$
      \item The value of $\det{A}$ does not change if we add a constant multiple of another row.
      \item If we exchange rows, the sign of $\det{A}$ flips
      \item The above hold for columns as well
    \end{itemize}

    Though not algorithmically sound (it is far too complex, cumbersome to
    program and certainly not stable\footnote{stable in the sense that
    floating point operations are prone to error and that error propagates})
    Cramer's Rule does give us an explicit solution for a system of linear
    equations:
    $$x_1 = \frac{\det{A_1}}{\det{A}}, x_2 = \frac{\det{A_2}}{\det{A}}, \ldots
    x_n = \frac{\det{A_n}}{\det{A}}$$
    where $A_j$ is the matrix obtained by replacing the $j$-th column of $A$ by
    the vector $b$.

\section{Linear Programming}
\index{Linear Programming}

A \emph{linear program} is mathematical model for linear optimization problems.
\begin{itemize}
  \item There is some \emph{objective} function which you wish to maximize (or minimize, called the \emph{dual})
  \item There are \emph{constraints} that cannot be violated
  \item Both must be \emph{linear} functions
  \item In general there can be $n$ variables
  \item Objective: output the value of $n$ variables that is both \emph{feasible} and \emph{optimizes} the outcome
\end{itemize}

A linear program in \emph{standard form}:

$$
  \begin{array}{lrcl}
    \textrm{maximize}   & & \vec{c}^{\,\, T} \cdot \vec{x} &    \\
    \textrm{subject to} & Ax & \leq  & \vec{b} \\
			    & \vec{x} & \geq & 0
  \end{array}$$


\subsubsection{Simplex Method: Example}

The \emph{simplex method} (Dantzig, 1947) is an old way of solving linear programs
\begin{itemize}
  \item Start at an arbitrary point
  \item Travel along the \emph{feasible region} to the next vertex (in $n$-space)
  \item In general, the optimal solution lies on the \emph{polytope} with ${n \choose m}$ extreme points ($n$ variables, $m$ constraints)
  \item May have an exponential number of such critical points
  \item Choice of direction: that which improves the solution the most (the direction in which the variable has the largest partial derivative)
  \item In general, pivot choices may lead to aberrant (exponential) behavior or could even cycle
  \item Very fast in practice
  \item Polynomial-time algorithm (Khachiyan, 1979) and better interior-point method (Karmarkar, 1984) among other improvements
\end{itemize}

$$
  \begin{array}{lrcl}
    \textrm{maximize}   & 4x + 3y &  &   \\
    \textrm{subject to} & x & \geq & 0 \\
			    & y & \geq & 0 \\
			    & x & \leq  & 9 \\
			    & 2y + x & \leq & 12 \\
			    & 2y - x & \leq & 8 \\
  \end{array}$$

\begin{figure}[h]
\centering
\input{figures/figLinearProg}
\caption{Visualization of a Linear Program with two sub-optimal solution lines and the optimal one.}
\label{fig:linearProgram}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{cc}
Point & Value \\
\hline\hline
$(0,0)$ & $0$ \\
$(0,4)$ & $12$ \\
$(2,5)$ & $23$ \\
$(9,1.5)$ & $40.5$ \\
$(9,0)$ & $36$ \\
\end{tabular}
\caption{Function values at several critical points}
\label{table:linearProgramSolutions}
\end{table}

\subsection{Formulations}

We can transform and conquer several previously examined problems as follows: we can reformulate the problem
as an optimization problem (a linear program) and solve it with the simplex method or other algorithm for LP.

\subsubsection{Knapsack}

The Knapsack problem can be formulated as a linear program as follows.
\begin{itemize}
  \item We want to maximize the sum of the value of all items
  \item Subject to the total weight constraint
  \item \emph{But} we want to constrain the fact that we can take an item or not
  \item We establish $n$ \emph{indicator variables}, $x_1, \ldots, x_n$, 
	$$ x_{i} \left\{ \begin{array}{rl}
	  1 & \textrm{ if item $i$ is taken} \\
	  0 & \textrm{ otherwise}
	\end{array}\right.$$
\end{itemize}


\begin{align*}
  \textrm{maximize}   & \quad \sum_{i=1}^n v_i\cdot x_i  \\
  \textrm{subject to} & \quad \sum_{i=1}^n w_i\cdot x_i \leq  W \\
  ~  &  \quad  x_i \in \{0, 1\} \quad 1 \leq i \leq n
\end{align*}


\begin{itemize}
  \item This is known as an \emph{Integer Linear Program}
  \item Some constraints are further required to be integers
  \item In general, ILP is $\NP$-complete
  \item A linear program can be \emph{relaxed}:
  \item Integer constraints are relaxed and allowed to be continuous
\end{itemize}

An example: the fractional knapsack problem:

\begin{align*}
  \textrm{maximize}   & \quad \sum_{i=1}^n v_i\cdot x_i  \\
  \textrm{subject to} & \quad \sum_{i=1}^n w_i\cdot x_i \leq  W \\
  ~  &  \quad 0 \leq x_i \leq 1 \quad 1 \leq i \leq n
\end{align*}

\begin{itemize}
  \item This formulation doesn't solve the original 0-1 Knapsack problem, it
  	could come up with a solution where a fraction of an item is taken
\end{itemize}

%\subsubsection{Graph: Shortest Path}
%
%Given a directed weighted graph $G = (V,E)$ and two vertices $s, t \in V$ (source, target) we want to find the
%shortest weighted path from $s$ to $t$.
%
%\begin{itemize}
%  \item Let $w_{ij}$ be the weight of the edge from $v_i$ to $v_j$
%  \item Wish to minimize the total weight of edges from $s$ to $t$
%  \item Consider a collection of \emph{indicator} variables:
%	$$ x_{ij} \left\{ \begin{array}{rl}
%	  1 & \textrm{ if edge $(v_i, v_j)$ is taken} \\
%	  0 & \textrm{ otherwise}
%	\end{array}\right.$$
%  \item We also want to constrain to \emph{valid paths}: if a vertex is entered, it should be exited (except $s, t$)
%  \item In the last constraint below, the first sum represents the out-degree of a vertex $v_i$, the second represents
%	the in-degree; for all vertices the difference should be zero except for $s,t$ which should be $1, -1$ respectively
%\end{itemize}
%
%\begin{align*}
%  \textrm{minimize}   & \quad  \sum_{1\leq i,j\leq n} w_{ij} \cdot x_{ij}  \\
%  \textrm{subject to} & \quad x_{ij} \geq 0 \\
%  ~  &  \quad \forall i \quad \sum_{j} x_{ij} - \sum_{j} x_{ji} =
%	\left\{\begin{array}{rl}
%	1 & \textrm{if } i = s\\
%	-1 & \textrm{if } i = t\\
%	0 & \textrm{otherwise}
%	\end{array}\right.
%\end{align*}

\subsubsection{Assignment Problem \& Unimodularity}
\index{Assignment Problem}\index{Problems!Assignment Problem}

Recall the assignment problem; we can formulate an ILP as follows

\begin{align*}
  \textrm{minimize}   & \quad \sum_{i}\sum_{j} c_{i,j} \cdot x_{i,j}  \\
  \textrm{subject to} & \quad \sum_{i} x_{i,j} = 1 \quad \forall j \\
  ~  &  \quad  x_{i,j} \in \{0, 1\} \quad \forall i,j
\end{align*}

Where $x_{i,j}$ is an indicator variable for assigning task $i$ to person $j$ (with associated cost $c_{i,j}$)

This program has integer constraints (ILP) and is, in general, $\NP$-complete.  We could relax it:

\begin{align*}
  \textrm{minimize}   & \quad \sum_{i}\sum_{j} c_{i,j} \cdot x_{i,j}  \\
  \textrm{subject to} & \quad \sum_{i} x_{i,j} = 1 \quad \forall j \\
  ~  &  \quad 0 \leq x_{i,j} \leq 1  \quad \forall i,j
\end{align*}

\begin{itemize}
  \item It doesn't make much sense to assign half a task to a quarter of a person (or maybe it does!)
  \item However, it turns out that certain problems are \emph{unimodular}
  \item A matrix is \emph{unimodular} if it is:
	\begin{itemize}
	  \item square
	  \item integer entries
	  \item has a determinant that is $-1, 1$
	\end{itemize}
  \item Further it is \emph{totally unimodular} if every square non-singular submatrix is unimodular (thus determinant is $0, -1, 1$)
  \item Fact: if an ILP's constraint matrix is totally unimodular, then its optimal solution is integral
  \item If an optimum exists, it is composed of integers: $\vec{x} = (x_1, \ldots, x_n)$, $x_i \in \mathbb{Z}$
  \item Fact: The assignment problem is \emph{totally unimodular}
  \item Consequence: the relaxation of the assignment problem's ILP will have the \emph{same optimal solution} as the original ILP!
\end{itemize}

    
\clearpage
\section{Exercises}

\begin{exercise}
Let $T$ be a binary search tree and let $v_1, v_2, \ldots, v_n$ be a preorder
traversal of its nodes.  Design an algorithm that reconstructs the tree $T$
given $v_1, \ldots, v_n$.
\end{exercise}

\begin{exercise}
Diagram each step of inserting the following keys into an
initially empty AVL tree.  Clearly indicate which type of rotations occur.
  $$10, 15, 30, 25, 5, 20, 35, 40, 45, 100, 50$$
\end{exercise}

\begin{exercise}
Diagram each step of inserting the following keys into an
initially empty 2-3 tree.  Clearly indicate which type of promotions and
node transformations occur.
  $$10, 15, 30, 25, 5, 20, 35, 40, 45, 100, 50$$
\end{exercise}

\begin{exercise}
Consider the following questions regarding AVL trees.
\begin{enumerate}[(a)]
  \item Considering adding the keys $a, b, c$ to an empty AVL tree.  How many
  	possible orders are there to insert them and how many of those do \emph{not}
	result in any rotations?
  \item Considering adding keys $a, b, c, d, e, f, g$ into an AVL tree.  How many 
  	ways could you insert these keys into an empty AVL tree such that no
	rotations would result?
  \item Can you generalize this for keys $k_1, \ldots, k_m$ where $m = 2^n - 1$?
\end{enumerate}
\end{exercise}

\begin{solution}
\begin{parts}
  \part There are $3!=6$ ways to insert, 2 do not lead to any rotations ($b, a, c$; $b, c, a$).
  	The remaining 4 lead to each of the 4 types of rotation.
  \part There are only 2 ways to insert the first three keys as in the previous part, which 
  	must be $d, b, f$ or $d, f, b$.  Once we have a full tree of 3 keys (no rotations), then
	the remaining 4 can be inserted in any order, so $2 \cdot 4! = 48$.
\end{parts}
\end{solution}

\begin{exercise}
Suppose we insert $1, 2, \ldots, 25$ into an initially empty AVL tree.  
What will be the depth of the resulting tree?
\end{exercise}

\begin{exercise}
Suppose we insert $1, 2, \ldots, 25$ into an initially empty 2-3 tree.  
What will be the depth of the resulting tree?
\end{exercise}

\begin{exercise}
Write an algorithm (give good pseudocode and a complete analysis) 
  	that \emph{inverts} a binary search tree so that for each node with key 
	$k$, all keys in the left-sub-tree are greater than $k$ and all nodes in 
	the right-sub-tree are less than $k$.  Thus, an in-order traversal of an 
	inverted tree will produce a key ordering in non-increasing order instead.
	An example of a binary search tree and its inversion is presented in Figure 
	\ref{figure:bsts}.  Your algorithm must be efficient and should not create 
	a new tree.
	\input{figures/figBSTInversion}
\end{exercise}

\begin{exercise}
Implement Gaussian Elimination in the high-level programming language
  	of your choice.  Use your code to solve matrix problems: finding a matrix
	inverse, solving a system of linear equations, etc.
\end{exercise}
