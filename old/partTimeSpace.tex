
Motivation:
\begin{itemize}
  \item Linked lists provide $O(n), O(1)$ access/insert efficiency
  \item Array-based lists provide $O(1), O(n)$ access/insert efficiency
  \item Balanced binary search trees provide $O(\log{n})$ operations
  \item Hash tables provide \emph{amortized} $O(1)$ operations
\end{itemize}

Basics:
\begin{itemize}
  \item Elements are stored in a fixed-size array
  \item The location of an element (its index) is computed by computing a \emph{hash} value of the element
  \item We can restrict attention to integer keys (elements/objects can be mapped to an integer key based on their state)
  \item Computing the hash value of a key provides quick, random access to the associated bucket containing the element (value)
  \item Insertion and retrieval are achieved by computing the hash value and accessing the array
\end{itemize}

\subsection{Hash Functions}

\begin{itemize}
  \item Simply put, a hash function is a function that maps a large domain to a small co-domain
  \item For our purposes:
	$$h: \mathbb{Z} \rightarrow \mathbb{Z}_m$$
	where $\mathbb{Z}_m = \{0, 1, 2, \ldots, m-1\}$
  \item Hash functions should be
	\begin{enumerate}
	  \item Easy to compute
	  \item Distribute values as uniformly as possible among $\mathbb{Z}_m$
	\end{enumerate}
\end{itemize}

\textbf{Example}. Consider the hash function:
  $$h(k) = 7k + 13 \bmod{8}$$
Insertion of the following keys into a hash table with the above has function results in the following configuration.
  $$10, 16, 4, 13, 86$$

\begin{table}[h]
\centering
\begin{tabular}{|r|c|c|c|c|c|c|c|c|}
\hline
Index & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
\hline
Value & 13 & 4 & ~ & 10 & ~ & 16 & ~ & 86 \\
\hline
\end{tabular}
\caption{Resulting Hash Table}
\label{table:hashTable}
\end{table}

But, now suppose we attempt to insert the key 100: $h(100) = 1$.  The array cell is already occupied by
4.  This is known as a \emph{collision}

\subsection{Collisions}

\begin{itemize}
  \item By definition, hash functions map large sets to small sets; they cannot be one-to-one
  \item Some elements $k_1, k_2$ will necessarily map to the same hash value $h(k_1) = h(k_2)$
  \item When two elements map to the same hash value, it is called a \emph{collision}
  \item Collisions can be \emph{resolved} using various strategies; we examine two:
  \begin{enumerate}
    \item Open addressing
    \item Chaining
  \end{enumerate}
\end{itemize}

\subsubsection{Open Addressing}

Open addressing involves \emph{probing} the hash table to find an open cell in the table.  Several
strategies are available.

\begin{itemize}
  \item Linear probing involves probing the table using a linear function:
	$$h(k,i) = h(k) + c_1 \cdot i \bmod{m}$$
	for $i = 1, \ldots, m-1$ and $c_1 \in \mathbb{Z}_m$
  \item Quadratic probing involves probing the table using a quadratic function:
	$$h(k,i) = h(k) + c_1 \cdot i + c_2 \cdot i^2 \bmod{m}$$
	for $i = 0, 1, \ldots, m$
  \item In general, $c_1, c_2 \in \mathbb{Z}_m$
  \item Probing wraps around the table (modulo $m$)
  \item Double Hashing involves using a second hash function $s(k)$ to probe:
	$$h(k,i) = h(k) + i\cdot s(k) \bmod{m}$$
	for $i = 0, 1, \ldots, m$
\end{itemize}

Retrieval and Deletion
\begin{itemize}
  \item Retrieval now involves hashing \emph{and} resolving collisions until either the element is found or
	a free cell is found (item is not in the table)
  \item If deletions are allowed, additional bookkeeping is needed to indicate if a cell has \emph{ever} been occupied; sometimes referred to as a ``dirty bit''
\end{itemize}

\subsubsection{Chaining}

\begin{itemize}
  \item No probing, instead each bucket in the hash table represents a linked list
  \item Alternatively: another efficient collection (BST, etc.)
  \item Each element is added to the list associated with the hash value
  \item Retrieval now may involve traversing another data structure
  \item Retrieval may now also involve another mechanism to identify each object in the collection
  \item No more issues with deletion
  \item Rehashing may still be necessary to prevent each list/collection from getting too large
\end{itemize}

\subsection{Efficiency Rehashing}

Efficiency of a hash table depends on how full it is.  Let $n$ be the number of elements in the table, then
the \emph{load factor} of the table is
$$\alpha = \frac{n}{m}$$

For successful searches, the expected number of key comparisons is
  $$S = 1 + \frac{\alpha}{2}$$
The fuller a table gets, the more its performance degrades to linear search:
\begin{itemize}
  \item Smaller table $\rightarrow$ more collisions $\rightarrow$ slower access/insert (but less wasted space)
  \item Larger table $\rightarrow$ fewer collisions $\rightarrow$ faster access/insert (but more wasted space)
\end{itemize}

This represents a fundamental time/space trade-off.

\begin{itemize}
  \item As a hash table fills up, more collisions slow down access of elements
  \item If deletion is allowed, more cells become dirty and slow down access of elements
  \item To accommodate more elements and to ensure that insert/retrieval remains fast a \emph{rehashing} of the tables
  \begin{enumerate}
    \item A new, larger table is created with a new hash function using a larger $m$
    \item Each element is reinserted into the new hash table, an $O(n)$ operation.
  \end{enumerate}
  \item Rehashing is done infrequently (when the load factor is exceeded): when averaged over the lifecycle of the 
  	data structure, it leads to \emph{amortized} constant time.
\end{itemize}

\subsection{Other Applications}

\begin{itemize}
  \item Pseudorandom number generators
  \item Data Integrity (check sums)
  \item Cryptography: message authentication codes, data integrity, password storage (ex: MD4, 5, SHA-1, 2, 3)
  \item Derandomization
  \item Maps, Associative Arrays, Sets, Caches
\end{itemize}

\subsection{Java Implementations}

The Java Collections library provides several data structures that are backed by hash tables that
utilize an object's \mintinline{java}{hashCode()} method and rely on several assumptions.  For this reason,
it is best practice to always override the \mintinline{java}{hashCode()} and \mintinline{java}{equals()} methods for
every user-defined object so that they depend on an object's entire state.

\begin{itemize}
  \item \mintinline{java}{java.util.HashSet<E>}
  \begin{itemize}
    \item Implements the \mintinline{java}{Set} interface
    \item Permits a single \mintinline{java}{null} element
  \end{itemize}
  \item \mintinline{java}{java.util.Hashtable<K,V>}
  \begin{itemize}
    \item A chained hash table implementation
    \item Documentation labels this implementation as ``open'' (actually refers to ``open hashing'')
    \item Allows you to specify an initial capacity and load factor with defaults ($.75$)
    \item Automatically grows/rehashes (\mintinline{java}{rehash()})
    \item Implements the \mintinline{java}{Map} interface
    \item Synchronized for multithreaded applications
    \item Does not allow \mintinline{java}{null} keys nor values
  \end{itemize}
  \item \mintinline{java}{java.util.HashMap<K,V>}
  \begin{itemize}
    \item Implements the \mintinline{java}{Map} interface
    \item Allows at most one \mintinline{java}{null} key, any number of \mintinline{java}{null} values
    \item Subclass: \mintinline{java}{LinkedHashMap} (guarantees iteration order, can be used as an LRU cache)
  \end{itemize}
\end{itemize}


