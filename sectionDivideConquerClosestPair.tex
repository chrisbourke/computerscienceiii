%!TEX root = ComputerScienceThree.tex

\section{Closest Pair: A Divide \& Conquer Approach}
\index{closest pair problem}

Consider again the problem of finding the two closest points in a set
of points in the Euclidean plane.  Specifically:

\textbf{Given}: a set of $n$ points 
 $$P = \{p_1=(x_1,y_1), \ldots, p_n = (x_n,y_n)\}$$
\textbf{Find}: the pair of points $(p, q) = (x,y), (x',y')$ that are the closest
with respect to Euclidean distance:
  $$d(p, q) = \sqrt{ (x-x')^2 + (y-y')^2}$$

Recall that the brute force approach we developed in Section 
\ref{subsection:ClosestPair} was a quadratic, $O(n^2)$ algorithm because
we exhaustively examined the distance between all pairs.  This
approach didn't exploit any underlying structure of the problem.
We now present a divide-and-conquer approach that \emph{does}
exploit the underlying geometry of the distance problem.  Ultimately,
we can avoid computing and examining a substantial number of
pairs that are ``far away'' from each other.  

The divide and conquer strategy will take the following overall
strategy:

\begin{itemize}
  \item \textbf{Divide} the set of points into two \emph{partitions}
  roughly the same size.  In particular, we'll assume that the points 
  are sorted with respect to their $x$ coordinates.  This gives us
  a natural division into a ``left'' and a ``right'' partition:
	$$\begin{array}{rcl}
	  L & = & \{(x_1, y_1), (x_2, y_2), \ldots (x_{n/2}, y_{n/2})\} \\
	  R & = & \{(x_{n/2+1}, y_{n/2+1}), \ldots (x_{n}, y_{n})\}
	\end{array}$$
  \item \textbf{Conquer} the problem by recursively finding the 
  two closest pairs in $L$ and $R$
\end{itemize}

An obviously trivial \emph{base case} is when we have only 2 points
which are, by definition, the two closest points.  

Suppose that $(a,b)$ are the two closest points in the left 
partition, $L$ with a distance $d_1$ and that $(\alpha,\beta)$ 
are the two closest points in the right partition, $R$ with
distance $d_2$.  Between these two pairs of points, we'll choose the pair that 
are closest to each other.  Without loss of generality, suppose that the
two closest pair are $(p,q)$ with a distance of $\delta$.

There is a problem, however.  By separating the points into two
partitions, we are potentially missing a \emph{closer} pair of
points such that one point is in $L$ and the other point is in
$R$.  We need to ensure that we are considering any such pair
of points.

\subsection{The Naive Approach}

Consider the following naive approach.  For each point in $L$, 
we'll compute and consider the distance to all other points in
$R$.  This gives us the following subroutine.

\begin{algorithm}[H]
  \ForEach{$(x,y) \in L$}{
    \ForEach{$(x',y') \in R$}{
      \If{$d( (x,y), (x',y') ) < \delta$}{
        \Comment{update the closest pair and closest distance}
        $(p, q) \leftarrow (x,y), (x',y')$ \;
        $\delta \leftarrow d( (x,y), (x',y') )$ \;
      }
    }
  }
\end{algorithm}

The size of each partition, $L, R$ is $\frac{n}{2}$ and so this 
subroutine would make 
  $$\frac{n}{2} \cdot \frac{n}{2} = \frac{n^2}{4} \in O(n^2)$$
comparisons (and distance calculations).  This gives us the following
analysis for the full algorithm.  Let $C(n)$ be the number of
comparisons (or distance calculations) for the overall recursive
divide-and-conquer approach.  We make two recursive calls on 
the two partitions each of size $\frac{n}{2}$.  As noted above,
non-recursively we perform $\frac{n^2}{4}$ operations giving
us the following recurrence.
  $$C(n) = 2C\left(\frac{n}{2}\right) + \frac{n^2}{4}$$
Applying the Master Theorem, we have that 
  $$C(n) \in O(n^2)$$
which is no better than the brute-force solution.

\subsection{Limiting Points: Exploiting Geometry}
\label{subsection:limitingPoints}

In order to achieve an improvement over the naive approach, we
have to limit the number of point distances we consider.  To
do this, we will exploit geometry.  

Consider again, the split of the set of points into the left
and right partition along an $x$-coordinate $m$.  We've already
found a pair of points $(p,q)$ with minimum distance $\delta$.
Now consider a point $(x,y)$ in the left partition.  The naive
approach would have us compute the distance from this point to
\emph{every} point in the right partition even if it is \emph{very
far away}.  ``Very far'' here means a distance greater than 
the minimum distance $\delta$ that we've found so far.  Clearly,
we can exclude such points.  

Consider Figure \ref{fig:figClosestPairSlices}: we have the 
dividing line $m$ and we've drawn two vertical borders $\delta$
distance to either side of $m$.  We observe that we only need
to consider pairs from the \emph{left slice} (defined by $m-\delta$) 
to the \emph{right slice} (defined by $m+\delta$). We don't 
have to consider any points to the left or right of these slices!

\begin{figure}
\centering
\includestandalone[width=0.35\textwidth]{figures/figClosestPairSlices}
\label{fig:figClosestPairSlices}
\caption{Limited area crossing the median split}
\end{figure}

However, that's still not enough.  It could be the case that ``a
lot'' of points lie within these two regions (a linear proportion of 
the $\frac{n}{2}$ or more generally $O(n)$ points) which results 
yet again in an $O(n^2)$ algorithm overall.  To solve this problem, 
we \emph{also} limit the area in the left slice with respect to the
$y$-coordinates.  Again, consider our point $(x,y)$ that may lie
arbitrarily close to our dividing line $m$.  We then draw a
region limited in the $y$-direction a distance $\delta$ in both
directions (defined as $[y - \delta, y + \delta]$).  We now only
have to consider points within this $\delta \times 2\delta$ sized 
region.  Any points outside this region are necessarily a distance
greater than $\delta$.  

We now observe that there are a \emph{limited} number of points we
can fit in this region.  Loosely speaking (see Section 
\ref{subsubsection:notOnLimitingPoints}) there are at most 6 
points that we could fit in this region; one at the corners which
are $\delta$ distance from each other (the blue circled areas).  
If we were to draw any other point within this region it would 
necessarily be distance \emph{strictly less} than $\delta$ and
so our algorithm \emph{should} have found this closer pair
when it recursed on the right partition.

This means we can limit our previous subroutine as follows.

\begin{algorithm}[H]
  \ForEach{$(x,y) \in L$}{
    \ForEach{$(x',y')$ within the $\delta \times 2\delta$ rectangle}{
      \If{$d( (x,y), (x',y') ) < \delta$}{
        $(p, q) \leftarrow (x,y), (x',y')$ \;
        $\delta \leftarrow d( (x,y), (x',y') )$ \;
      }
    }
  }
\end{algorithm}

This is essentially the same routine but the inner loop is now
guaranteed to run in \emph{constant} time because there are 
at most 6 such points to iterate over.  We've successfully 
exploited the geometric structure of the problem to limit the
number of point pairs we have to consider.  This subroutine now 
has a complexity of:
  $$\frac{n}{2} \cdot 6 = 3n \in O(n)$$
and our recurrence is now
  $$C(n) = 2C\left(\frac{n}{2}\right) + 3n$$
Applying the Master Theorem, we have that 
  $$C(n) \in O(n\log{(n)})$$
or quasilinear.  This represents a huge improvement over the
naive brute force algorithm both theoretically and in practice.

There are some practical details we need to consider, however.


\subsection{Implementation}

The full pseudocode for our divide and conquer algorithm is 
presented in Algorithm \ref{algo:recursiveClosestPair}.

\begin{algorithm}[H]
  \Input{A set of points $P = \{(x_1,y_1), \ldots, (x_n,y_n)\}$}
  \Output{$(p, q)$ the closest pair of points in $P$}
  \If{$n \leq threshold$}{
    return closest pair in $P$ using the brute force algorithm \;
  }
  Evenly split $P$ into a left $L$ and right $R$ partition with respect to the point's $x$ coordinates \;
  Let $m$ be the $x$-coordinate that splits the two partitions \;
  $(a,b) \leftarrow \textsc{RecursiveClosestPair}(L)$ \;
  $(\alpha,\beta) \leftarrow \textsc{RecursiveClosestPair}(R)$ \;
  \Comment{$(p,q)$ will be the two closest pair between the closest pair from each partition with distance $\delta$}
  $(p,q) \leftarrow \mathrm{argmin}\{d(a,b), d(\alpha,\beta)\}$ \;
  $\delta \leftarrow d(p,q)$ \;
  Let $\textrm{leftSlice} \subseteq L$ be the set of all points whose 
  $x$ coordinates are in $[m - \delta, m]$ \;
  Let $\textrm{rightSlice} \subseteq R$ be the set of all points whose 
  $x$ coordinates are in $[m, m + \delta]$ \;
  \ForEach{$(x,y) \in \textrm{leftSlice}$}{
    \ForEach{$(x',y')$ in \textrm{rightSlice} with $y$-coordinates in $[y - \delta, y+\delta]$}{
      \If{$d( (x,y), (x',y') ) < \delta$}{
        $(p, q) \leftarrow (x,y), (x',y')$ \;
        $\delta \leftarrow d( (x,y), (x',y') )$ \;
      }
    }
  }
  return $(p,q)$ \;

  \caption{\textsc{RecursiveClosestPair}$(S)$}
  \label{algo:recursiveClosestPair}
\end{algorithm}

In an actual implementation of this algorithm there are several
details/pitfalls that you should be aware of.

\begin{itemize}
  \item For the base case, you can define a \emph{threshold} such that if 
  the number of is fewer than (say) 20 (or some other value), then you can
  halt the recursion and use the brute force, all-pairs algorithm.  This 
  means you don't have to worry about the details of splits that don't result
  in \emph{exactly} two points.  It can also be faster in practice.  At 
  some point, the cost of recursion exceeds the speed-up of the 
  divide \& conquer strategy.  This is a common strategy in recursive,
  \emph{hybrid} algorithms.

  \item Recall that we assumed that the points in $P$ were presorted 
  with respect to their $x$ coordinates.  Prior to the first recursion,
  you should ensure this by explicitly sorting the points.   This makes 
  the partitioning step (line 3) trivial.  Computing $m$ also becomes
  very easy (by taking the last point in $L$ and the first point in $R$
  and computing the average of their $x$ coordinates.  
  
  \item If the points are sorted with respect to their $x$ coordinates, 
  it also makes building the left/right slices (lines 9--10) easy 
  as well.  You can use a (modified) binary search to find the index
  $i$ such that all points with index $i$ or greater in $L$ have an $x$ value
  greater than (\emph{or equal to}) $m - \delta$.  
  
  Likewise, you can use binary search to find the index $j$ such that
  all points in $R$ with index $j$ or less have an $x$ value less than
  (\emph{or equal to}) $m + \delta$.  Of course it is also convenient
  to maintain the left and right slices separately (which is natural
  given that the set of points has already been split into left/right
  partitions).
  
  \item Now for a key implementation detail.  Conceptually, we limited
  the number of points in the right slice (the $\delta \times 2\delta$
  area rectangle), but we left out the details about how to 
  \emph{efficiently} find all points.  If we were to naively search
  all points in the right slice, we're right back to the $O(n^2)$ 
  complexity.  
  
  To efficiently limit the number of points we consider in line 12, 
  we need the points to be sorted with respect to their $y$ 
  coordinates.  Then we can use the same (modified) binary search
  described above to find indices $(i,j)$ such that the $y$
  coordinates of all points with indices $i$ thru $j$ lie within
  $[y-\delta, y+\delta]$.
  
  This can be achieved in one of two ways.  In the original algorithm,
  two copies of the set of points are maintained: one sorted by
  their $x$-coordinates and one sorted by their $y$-coordinates 
  prior to the first recursion (see \cite{10.5555/1614191} for 
  details).  When the points are split into two partitions, 
  the $y$-sorted list is also split.  This can be done with
  $O(n)$ comparisons to determine which partition they belong in 
  and so does not change the recurrence we derived above and 
  maintains the optimal $O(n\log{(n)}$ complexity.
  
  Alternatively (and easier) is to simply (re)sort the list
  of points with respect to their $y$-coordinates as needed.
  In the pseudocode of Algorithm \ref{algo:recursiveClosestPair}
  between lines 10 and 11, we would sort the right slice 
  by their $y$ coordinates (there is no need to sort the
  left slice as we iterate over all the points anyway).  
  
  This alternative approach is easier as far as a practical
  implementation goes, but it does come at a cost.  There are
  potentially $\frac{n}{2}$ in the right slice and using an
  efficient sorting algorithm would require $O(n\log{(n)})$
  comparisons.  Unlike the optimal method, this is done 
  on every recursion so instead of a linear non-recursive
  cost, we have the following recurrence:
  
  $$C(n) = 2C\left(\frac{n}{2}\right) + 3n + \frac{n}{2}\log{\left(\frac{n}{2}\right)}$$

  The usual Master Theorem would not apply in this case since
  the non-recursive cost is not bound by a polynomial but a 
  polylogarithmic function.  Fortunately, there is a general
  form of the Master Theorem that allows us to characterize
  this recurrence as:
  $$C(n) \in O(n \log^2{(n)}$$
  which is slightly less efficient than the optimal $O(n\log{(n)})$
  but only by a logarithmic factor.  In practice this is 
  inconsequential given the arguably easier implementation.  
  
  \item As mentioned above, when restricting the points (building
  the left/right slices and limiting the right slice to the
  rectangle), it simplifies things greatly if you develop a (modified) binary 
  search that, given an array $A$ and a general $[lower, upper]$ range, 
  returns indices $(i, j)$ such that all elements $A[i]$ (inclusive) 
  thru $A[j]$ (exclusive) are within the interval $[lower, upper]$.  
  Details for achieving this are left as an exercise.  
  
\end{itemize}

\subsubsection{A Note On Limiting Points}
\label{subsubsection:notOnLimitingPoints}

The divide and conquer approach to this problem was originally presented
by Bentley and Shamos \cite{10.1145/800113.803652} and reproduced in the 
monograph \cite{preparata1993computational}.  In section 
\ref{subsection:limitingPoints} we've reproduced the same 
presentation from the original papers and standard textbooks
\cite{Levitin:2002:IDA:579301,10.5555/1614191}, arguing that we 
need only consider at most 6 points.  In fact, we can argue that
at most only 5 points need to be considered (and likely even
as few as 3).

%We can use a well-known result (from Crystallography)
%that limits the number of points $n$ that are a minimum distance $d$ from 
%each other that can be \emph{packed} into a polygon of area $A$:
%  $$n \leq \frac{4A}{\pi d^2}$$
%Substituting the area of a circle of radius $\delta$ and $d = \delta$, we
%get that $n \leq 4$.  In fact, we can reason that at most 3 points can
%fit into the semicircle on the other side of the dividing line.  

Since our measure of distance is the Euclidean distance, we 
only need to consider points that are within a (semi) circle
of radius $\delta$.  Consider a point on our dividing line
(see Figure \ref{fig:packingPointsCircle}) as the center
of our semicircle.  The optimal ``packing'' of points within
this semicircle is achieved by drawing 3 equilateral
triangles with sides equal to $\delta$.  The 5 points at
each corner ensure that they are a distance at least $\delta$
from each other.

\begin{figure}
\centering
\includestandalone[width=0.35\textwidth]{figures/figPackingPoints}
\label{fig:packingPointsCircle}
\caption{Packing up to 5 points in a semicircle of radius $\delta$ 
such that all points are at least $\delta$ distance from each other.}
\end{figure}

Likely we can restrict the number of points even further by 
observing that, to actually be \emph{closer} than $\delta$ to 
the center of our semicircle, all the points would have to 
lie \emph{strictly within} the border of this circle (as it 
is, all the points currently lie on the boundary).  With
that condition, it is likely that we can only end up fitting
\emph{one} of the equilateral triangles within the boundary
of the semicircle and thus only 3 points may be a distance
\emph{strictly} less than $\delta$ from our center.  

The typical presentations of this idea draw the less optimal
rectangle and argue that the limit is 6 for a couple of reasons.
First, the presentation (and understanding it) is much easier.
Moreover, we only need to limit the number of points to \emph{some}
constant for our Master Theorem analysis to hold.  That constant
is lost when we consider the asymptotic characterization. 

Another reason is that the original paper presented this idea
using a different definition of distance, the so-called
``Manhattan distance'' \index{Manhattan distance} 
(more formally the $\ell_1$ norm or
$L_1$ distance) which is defined as the absolute value of 
the difference of coordinates.
  $$|x_1 - x_2| + |y_1 - y_2|$$
The Manhattan distance is the distance you get when you have
to travel along a grid (instead of walking through the buildings
in Manhattan for a direct route).  The original paper then
noted that the same idea would work for any measure of distance
or even higher dimensional spaces.

%Chord Formula: given radius $r$ and distance from the center $d$, the 
%length of a chord is 
%  $$2 \sqrt{r^2-d^2}$$
%For $r = \delta, d = \frac{1}{2}\delta$, this gives
%  $$2 \sqrt{\delta^2 - (.5\delta)^2} = 2 \delta \sqrt{\frac{3}{4}}$$
%And so one half the chord is $\delta \sqrt{\frac{3}{4}}$



