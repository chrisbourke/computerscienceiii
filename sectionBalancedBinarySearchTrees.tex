
In a Binary Search Tree (BST), insert, delete, and search operations are proportional to the
\emph{depth} of the tree.
\begin{itemize}
  \item The depth $d$ of a tree is the length of the maximal path from the root to any leaf
  \item Ideally, in a full/complete binary tree, $d \in \Theta(\log{n})$
  \item In the worst case though, $d = n-1 \in \Theta(n)$
  \item Our goal is to maintain a BST's \emph{balance} so that the depth is \emph{always} $O(\log{n})$
  \item Many different types of \emph{self-balancing} binary search trees: 2-3 trees, 2-3-4 trees, 
  	B-trees, AA trees, AVL trees, Red-Black Trees, Splay Trees, Scapegoat Trees, Treaps, T-Trees, 
\end{itemize}

\subsection{AVL Trees}
\index{AVL Trees}\index{Balanced Binary Search Tree!AVL Trees}

Let $T$ be a binary tree with $u$ a node in $T$.  The \emph{height} of $u$
is the length of the longest path from $u$ to any descendant leaves in its
left or right subtree.  For example, in the tree in Figure \ref{fig:balanceFactorExample}, 
the node containing 6 has height 2 (as the longest path is from 6 to 4 which 
has length 2).  The node containing 8 has height 3 and the root has height 4.
Whereas the depth of a node is defined with respect to the root, the height of
a node is defined with respect to leaves.  The height of a tree (or subtree) is 
the height of its root node.  By convention, the height of a single node tree is 
0 and the height of an empty tree is -1.  

The balance factor of a node $u$ is equal to the height of $u$'s
left subtree minus the height of its right subtree.
  $$\textrm{balance factor}(u) = h(T_L) - h(T_R)$$
A balance factor is a measure of how skewed or unbalanced 
a tree or subtree is.  A balance factor of 0 indicates that a node
is balanced between its left/right subtree.  A ``large'' positive 
balance factor indicates a tree that is unbalanced to the left
while a large negative balance factor indicates a tree that is
unbalanced to the right.  A node's balance factor only 
quantifies how well balanced a tree is at that particular node, 
not overall.  That is, its a local property.  Figure \ref{fig:balanceFactorExample}
depicts a BST with the balance factor indicated for each node.
Observe that for the node containing 4, the left and right 
subtree both have a height of $-1$, so the balance factor is
$-1 - (-1) = 0$.  The balance factor of 8 is 3 since its left
subtree has a height of 2 and its right subtree has a height
of $-1$.

  \input{figures/avlBalanceFactorFig}

\begin{definition}{AVL Tree}
An \emph{AVL Tree} is a binary search tree in which the 
\emph{balance factor} of every node's left and right subtrees 
is 0, 1, or -1.
\end{definition}

AVL represents the developer's names (Adelson-Velsky and Landis 
\cite{AdelsonLandis62}).

Insertion into an AVL tree is done in the same way as a standard
BST.  However, an insertion may result in unbalanced nodes.  First, 
we observe that since we only insert one node at a time, a node's
balance factor can only ever become $+2$ or $-2$ (assuming it 
was an AVL tree to begin with).  If several nodes become unbalanced, 
it is enough to consider the unbalanced node closest to the new node
we just inserted.  Correcting the imbalance at that node should
correct the imbalance at other nodes.

We need to re-balance the tree to ensure the AVL Tree properties
are preserved after insertion.  To do this, we use one of several 
\emph{rotations} depending on the type of imbalance.

    There are four basic rotations:
    \begin{itemize}
      \item $R$ -- A right rotation is used when three nodes are skewed all
      the way to the right.  We rotate such that the middle one becomes the
      new root.
      \item $L$ -- A left rotation is used when three nodes are skewed all
      the way to the left.  We rotate such that the middle node becomes the
      new root.
      \item $LR$ -- A double left-right rotation is done when the middle
      node is skewed to the left and its child is skewed to the right.
      We left-rotate at the middle node and then right rotate at the top node.
      \item $RL$ -- A double right-left rotation is done when the middle
      node is skewed to the right and its child is skewed to the left.
      We right-rotate at the middle node and then left rotate at the top node.
    \end{itemize}

These rotations are depicted in simple trees in Figure \ref{fig:avlRotationLSimple}
through \ref{fig:avlRotationRLSimple} and the generalized 
rotations with subtrees present are presented in Figures \ref{fig:avlRotationL} 
through \ref{fig:avlRotationRL}.

\input{figures/avlRotationsFig}

To illustrate some of these operations, consider the example depicted 
Figure \ref{fig:avlOperations} where we insert the keys, 
$8, 4, 7, 3, 2, 5, 1, 10, 6$ into an initially empty tree.

\input{figures/avlOperationsFig}

Deleting from an AVL tree is the same as a normal BST.  Leaves can be
straightforwardly deleted, nodes with a single child can have the child
promoted, and nodes with both children can have their key replaced with
either the maximal element in the left subtree (containing the lesser
elements) or the minimal element in the right subtree (containing the
greater elements).  However, doing so may unbalance the tree.  
Deleting a node may reduce the height of some subtree which will
affect the balance factor of its ancestors.  The same rotations may
be necessary to rebalance ancestor nodes.  Moreover, rebalancing may
\emph{also} reduce the height of subtrees.  Thus, the rebalancing
may propagate all the way back to the root node.  An example of a 
worst case deletion is depicted in Figure \ref{fig:avlDeletionWorstCase}.

\input{figures/avlDeletionFig}

\subsubsection{Analysis}

The height $h$ of an AVL tree is bounded above and below:
  $$\lfloor \log_2{n}\rfloor \leq h \leq 1.4405\log_2{(n+2)} - 0.3277$$
Thus, no matter what the specific value of $h$ is for a given tree, we
can conclude that
  $$h \in \Theta(\log{n})$$
where $n$ is the number of nodes in the tree.  Furthermore, rotations
are simply a matter of switching out pointers ($O(1)$) thus
searching, insertion and deletion are all $O(h) = \Theta(\log{n})$ for AVL
Trees.

You can get some practice with AVL trees by trying out one of the 
many online simulations and animation applications.  For example:
\begin{itemize}
  \item \url{https://www.cs.usfca.edu/~galles/visualization/AVLtree.html}
  \item \url{http://www.qmatica.com/DataStructures/Trees/AVL/AVLTree.html}
\end{itemize}

\subsection{B-Trees}
\index{B-Trees}\index{Balanced Binary Search Tree!B-Trees}

\begin{itemize}
  \item Every node has at most $m$ children
  \item Every node (except the root) has at least $\lceil \frac{m}{2} \rceil$ children
  \item Root has at least 2 children unless it is a leaf
  \item Non-leaf nodes with $k$ children contain $k-1$ keys
  \item All leaves appear in the same level and are non-empty
  \item Commonly used in databases
  \item Special case: 2-3 Trees ($m = 3$)
  \item 2-3 Trees credited to Hopcroft (1970) and B-Trees credited to
  	Bayer \& McCreight (1972) \cite{Bayer:1970:OML:1734663.1734671}
\end{itemize}
\index{2-3 Trees}\index{Balanced Binary Search Tree!2-3 Trees}

    Rather than a single key per node, a 2-3 tree may have 1 or 2 keys
    per node.

    \begin{itemize}
      \item \textbf{2-node} -- A node containing a single key $k$ with two
      children, i.e. the usual type of node in a regular BST.
      \item \textbf{3-node} -- A node containing two keys, $k_1, k_2$ such that
      $k_1 < k_2$.  A 3-node has \emph{three} children:
      \begin{itemize}
        \item Left-Most-Child represents all keys less than $k_1$
	\item Middle-Child represents all keys between $k_1, k_2$
	\item Right-Most-Child represents all keys greater than $k_2$
      \end{itemize}
    \end{itemize}

    Another requirement or property of a 2-3 Tree is that all of its
    leaves are on the same level.  Every path from the root to
    any leaf will be have the same length.  This ensures that the
    height $h$ is uniform and balanced.

    Basic Operations
    \begin{itemize}
      \item Searching is done straight forward, but slightly different
      than regular BSTs
      \item Insertion is always done \emph{in a leaf}.
        \begin{itemize}
	  \item If the leaf is a 2-node, we insert by making it a 3-node
	  and ordering the keys.
	  \item If the leaf is a 3-node, we split it up--the minimal key
	  becomes the left-child, the maximal key becomes the right child
	  and the middle key is promoted to the parent node
	\end{itemize}
    \end{itemize}

    The promotion of a node to the parent can cause the parent to overflow
    (if the parent was a 3-node), and thus the process may continue upwards
    to the node's ancestors.

    Note that if the promotion is at the root, a \emph{new} node is created
    that becomes the new root.

\subsubsection{Analysis}

Let $T$ be a 2-3 tree of height $h$.  The smallest number of
keys $T$ could have is when all nodes are 2-nodes, which is 
essentially a regular binary search tree.  As the tree is full, there
would be at least
  $$n \geq \sum_{i=0}^h 2^i = 2^{h+1} - 1$$
keys.  Conversely, the largest number of keys would be 
when all nodes are 3-nodes and every node has 2 keys
and three children.  Thus, 
  $$n \leq 2\sum_{i=0}^h 3^i = 2\left( \frac{3^{h+1}-1}{2} \right) = 3^{h+1}-1$$
Combining these two bounds and solving for $h$ yields
  $$\left\lceil \log_3{\left(n+1\right)} \right\rceil - 1 \leq h \leq \left\lceil \log_2{(n+1)} \right\rceil - 1$$
Thus, $h \in \Theta(\log{n})$.

\input{figures/23treeOperationsFig}

    As before, these bounds show that searching, insertion and deletion are
    all $\Theta(\log{n})$ in the worst \emph{and} average case.

Deleting: deleting from an internal node: exchange it with the
maximal value in the left subtree (or minimal value in the right subtree) as is standard.
Such a value will always be in a leaf.

Deletion may propagate to the root, merging siblings, etc.

\input{figures/23treeDeletionFig}

\subsection{Red-Black Trees}
\index{Red-Black Trees}\index{Balanced Binary Search Trees!Red-Black Trees}

    \begin{definition}
    A \emph{Red-Black Tree} is a binary search tree in which every node
    has a single (unique) key and a color, either red or black.
    \begin{itemize}
      \item The root is black and every leaf is black (this can be made
      trivial true by adding sentinels).
      \item Every red node has only black children.
      \item Every path from the root to a leaf has the same number of
      black nodes in it.
    \end{itemize}
    \end{definition}

    The black height of a node $x$ is the number of black nodes in
    any path from $x$ to a root (not counting $x$), denoted $bh(x)$.

    \begin{lemma}
    In a Red-Black Tree with $n$ internal nodes satisfies the height
    property
    $$h \leq 2\log_2{(n+1)}$$
    \end{lemma}

    You can prove this by induction on the height $h$ with the base
    case being a leaf.

    As with AVL Trees, insertion and deletion of nodes may violate
    the Red-Black Tree properties.  Thus, we insert just like a
    regular BST, but we also perform rotations if necessary.

    In fact, we use the \emph{exact same} Left and Right rotations
    as are used in AVL Trees.  However, LR and RL rotations are
    unnecessary.

    The first step to inserting a new node is to insert it as a
    regular binary search tree and color it red.

    There are three cases for insertion.

    \begin{itemize}
      \item Case 1 -- If the inserted node $z$ has a red parent and a
      red uncle (the cousin node of $z$'s parent).  In this case we
      recolor $z$'s parent, uncle, and grandfather.  We then recurse
      back up and see if any properties are violated at $z$'s grandfather.

      \item Case 2 -- $z$'s parent is red but $z$'s uncle is black.  If
      $z$ is the right-sub-child of its parent, then we perform a Left
      rotation.  The new $z$ becomes the new left-sub-child of $z$.

      \item Case 3 -- $z$'s parent is red but $z$'s uncle is black.  $z$
      is the left-sub-child of its parent, so we perform a Right
      rotation.  The new $z$ becomes the new left-sub-child of $z$.

    \end{itemize}

    \begin{itemize}
      \item \url{http://mathpost.la.asu.edu/~daniel/redblack.html}
      \item \url{http://www.ececs.uc.edu/~franco/C321/html/RedBlack/redblack.html}
    \end{itemize}

\section{Splay Trees}

\url{https://awards.acm.org/award_winners/sleator_0340141}